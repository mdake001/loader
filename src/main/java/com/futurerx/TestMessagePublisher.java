package com.futurerx;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.activemq.ActiveMQConnection;

import com.futurerx.messaging.MessagePublisher;
import com.futurerx.messaging.activemq.ActivemqMessagePublisher;

public class TestMessagePublisher {
	public static void main(String[] argv) throws Exception {
		MessagePublisher pub = new ActivemqMessagePublisher();
		Map<String, String> config = new HashMap<String, String>();
		config.put("host", ActiveMQConnection.DEFAULT_BROKER_URL);
		config.put("transacted", "true");
		config.put("transaction_batch_size", "500");
		pub.init("TEST", config);
		long startTime = new Date().getTime();
		for(int i = 0; i < 10000; i++)
		{
			//Map<String, Object> payload = new HashMap<String, Object>();
			//payload.put(String.valueOf(i), new Integer(i));
			ParentObject payload = new ParentObject();
			payload.setAtt1(String.valueOf(i));
			payload.setAtt2(new Integer(i));
			payload.setAtt3(i);
			payload.setAttr4(new Date());

			List<ChildObject> children = new ArrayList<ChildObject>();
			{
				ChildObject child = new ChildObject();
				child.setAtt1(String.valueOf(i));
				child.setAtt2(new Integer(i));
				child.setAtt3(i);
				child.setAttr4(new Date());
				children.add(child);
			}
			{
				ChildObject child = new ChildObject();
				child.setAtt1(String.valueOf(i));
				child.setAtt2(new Integer(i));
				child.setAtt3(i);
				child.setAttr4(new Date());
				children.add(child);
			}
			payload.setChildren(children);
			pub.publish(payload);
			/*
			if(i == 90)
			{
				Thread.sleep(60000);
			}
			*/
		}
		pub.close();
		long endTime = new Date().getTime();
		System.out.println("Time Taken:" + (endTime - startTime));
	}
}
