package com.futurerx;

import java.io.Serializable;
import java.util.List;

public class RecordContainer implements Serializable{
	Integer fileLoadSummaryId;
	Long recordNumber;
	List<Object> data;
	public List<Object> getData() {
		return data;
	}
	public void setData(List<Object> data) {
		this.data = data;
	}
	public Integer getFileLoadSummaryId() {
		return fileLoadSummaryId;
	}
	public void setFileLoadSummaryId(Integer fileLoadSummaryId) {
		this.fileLoadSummaryId = fileLoadSummaryId;
	}
	public Long getRecordNumber() {
		return recordNumber;
	}
	public void setRecordNumber(Long recordNumber) {
		this.recordNumber = recordNumber;
	}
}
