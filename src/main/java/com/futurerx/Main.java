package com.futurerx;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.mail.MessagingException;

import org.apache.activemq.ActiveMQConnection;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.blackbear.flatworm.ConfigurationReader;
import com.blackbear.flatworm.FileFormat;
import com.blackbear.flatworm.MatchedRecord;
import com.futurerx.audit.models.FileLoadDetail;
import com.futurerx.batch.BatchSave;
import com.futurerx.batch.Counts;
import com.futurerx.batch.Validator;
import com.futurerx.jep.ExpressionProcessor;
import com.futurerx.messaging.MessageConsumer;
import com.futurerx.messaging.MessageHandler;
import com.futurerx.messaging.MessagePublisher;
import com.futurerx.messaging.activemq.ActivemqMessageConsumer;

@SuppressWarnings("unused")
public class Main implements MessageHandler {	
	private static String INSERT_PROCESS_HISTORY_QUERY = "insert into data_loading_audit.process_history (id, process_name, start_time,filename) values (default,?,?,?)";                                                                   
	private static String UPDATE_PROCESS_HISTORY_QUERY = "UPDATE data_loading_audit.process_history SET end_time = ?, records_processed = ?, records_success = ?, records_failed = ?, exception = ? WHERE process_name = ? AND start_time = ? AND filename = ?";                                                                   
	private static String INSERT_INTAKE_PROCESS_HISTORY_QUERY = "INSERT INTO data_loading_audit.file_load_summary (file_name, file_create_date, start_date, end_date, description, status, count_received_stage, count_failed, last_record_processed) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";                                                                   
	private static String UPDATE_INTAKE_PROCESS_HISTORY_QUERY = "UPDATE data_loading_audit.file_load_summary SET  end_date = ?, status = ?, count_received_stage = ?, count_inserted_stage = ?, count_failed = ? WHERE id = ?";                                                                   
	private static String UPDATE_INTAKE_PROCESS_VALIDATION_HISTORY_QUERY = "UPDATE data_loading_audit.file_load_summary SET  end_date = ?, status = ?, count_failed_stage = count_failed_stage + ?, last_record_processed = last_record_processed + ? WHERE id = ?";                                                                   
	private static String INSERT_INTAKE_ERROR_DETAIL = "INSERT INTO data_loading_audit.file_load_detail (file_load_summary_id, record_number, status, remark) VALUES (?, ?, ?, ?)";
	private static String GET_FILE_LOAD_SUMMARY_ID = "SELECT id FROM data_loading_audit.file_load_summary WHERE file_name = ? order by id desc limit 1";                                                                   

	List<Map<String, Object>> codeSets = null;
	
	private static Integer BATCH_SIZE  = 100;
	private static Integer threadPoolSize = 50;
	private static Integer loop = 0, linesToLoad = Integer.MAX_VALUE, linesToSkip = -1;
	private static String fail = "FAIL", warn = "WARN", companyName = "", format = "";
	private static Boolean preprocess = true, validation = true, usequeue=false, resume=false, sentMail = false, noHeader=false;
	//private static Boolean publish = true;
//	private static String processType = null;
	private static String queueName = null, sourceName = "";
	private ApplicationContext aContext = new ClassPathXmlApplicationContext("applicationContext.xml");
	private List<String> postprocessingSqlList = new ArrayList<String>();
	private Validator v = new Validator();
	private Map<String, Map<String, Object>> codes = new HashMap<String, Map<String,Object>>();;
	//private boolean finishQueue = false;
	private JdbcTemplate jdbcTemplate = null;
	private Counts counts = null;
	private Logger log = LoggerFactory.getLogger(Main.class);
	private List<Future<Counts>> threadResults = new ArrayList<Future<Counts>>();
	private ExecutorService pool = null;
	BatchSave bs = null;
	ExpressionProcessor ep = null;
	Map<String, Object> retValue = null;
	List<FileLoadDetail> errors = new ArrayList<FileLoadDetail>();
	Map<String, Long> fileStats = new HashMap<String, Long>();
	Map<String, Long> fileStatsLastRecord = new HashMap<String, Long>();
	private JdbcTemplate getJdbcTemplate() {
		if (jdbcTemplate == null) {
			jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		}
		return jdbcTemplate;
	}

	private void closeDBConn() {
		if (jdbcTemplate != null) {
			jdbcTemplate = null;
		}
	}
	public void initialize() {
		this.pool = Executors.newFixedThreadPool(threadPoolSize);
	}
	
	public void finish() {
		try {
			this.pool.shutdown();
			this.closeDBConn();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
	}

	private void process(final List<String> sqls) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		for (String sql: sqls) {
			try {
				jdbcTemplate.execute(sql);
			} catch (DataAccessException e) {
				System.out.println("Error executing : " + sql);
			}
		}
	}
	
	private void addShutdownHook()
	{
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() { 
				System.out.println("Shutting Down...");
				Iterator<String> fileStatsIt = fileStats.keySet().iterator();
				while(fileStatsIt.hasNext())
				{
					String key = fileStatsIt.next();
					Long count = fileStats.get(key);
					finishValidationProcessing(new Integer(key), count, new Long(0));
				}
				fileStatsIt = fileStatsLastRecord.keySet().iterator();
				while(fileStatsIt.hasNext())
				{
					String key = fileStatsIt.next();
					Long count = fileStatsLastRecord.get(key);
					System.out.println("KEY:" + key + ":" + count);
					finishValidationProcessing(new Integer(key), new Long(0), count);
				}
			}
		});
	}

	private Integer checkFairRun() {
		if (counts.getTotalRecords() > BATCH_SIZE*2) {
			if ((float)counts.getFailedRecords()/(float)(counts.getInsertedRecordStage()==0?counts.getTotalRecords():counts.getInsertedRecordStage())>.50) {
				return 1;
			}else if ((float)counts.getFailedRecords()/(float)(counts.getInsertedRecordStage()==0?counts.getTotalRecords():counts.getInsertedRecordStage())>.10) {
				return 2;
			}
		}
		return 0;
	}
	
	private void processFile(String loadType, String fileName) throws Exception {
		counts = new Counts();
		bs = new BatchSave(getJdbcTemplate(), aContext, BATCH_SIZE);
		ep = new ExpressionProcessor(null, this.getJdbcTemplate().getDataSource().getConnection());
		retValue = startProcessing(fileName, loadType);
		Timestamp startTS =	(Timestamp)retValue.get("time");
		Integer fileLoadSummaryId =	(Integer)retValue.get("id");
		
		fileName.substring(fileName.lastIndexOf('/')+1, fileName.length());
		
		if (format == "") {
			format = "futurerx";
		}
		if(companyName == "") {
			companyName = "enterprise";
		}
		
		String exception = "";
		ConfigurationReader parser = new ConfigurationReader();
		FileFormat ff = null;
		String path = "conf" + File.separator + format + File.separator + loadType + ".conf";

		File checkFile = new File(path);
		if (!checkFile.exists())
			path = "conf" + File.separator + companyName + File.separator + loadType + ".conf";
		
		checkFile = new File(path);
		if (!checkFile.exists())
			path = "conf" + File.separator + companyName + File.separator + loadType + ".conf.xml";

		checkFile = new File(path);
		if (!checkFile.exists())
			path = "conf" + File.separator + format + File.separator + loadType + ".conf.xml";

		System.out.println(path);
		ff = parser.loadConfigurationFile(path);
		if (ff == null) {
			throw new Exception("Unknown Load Type: " + loadType + ". Check if " + path + " file exists or is valid.");
		}
		
		File file = new File(fileName);
		InputStream in = new FileInputStream(file);
		InputStreamReader reader = new InputStreamReader(in, "ISO-8859-1"); 
		BufferedReader bufIn = new BufferedReader(reader);
		MatchedRecord results = null;
		long startTime = (new Date()).getTime();

//		Long recordsProcessed = 0L, recordsFailed = 0L, recordsPassed = 0L;
		Long lineCount = 0L;
		
		if (!noHeader) {
			try {
				results = ff.getNextRecord(bufIn);
				lineCount++;
			}
			catch (Exception e) {
				e.printStackTrace();
				addErrorToList(createError(fail, lineCount, e.getMessage(), fileLoadSummaryId));
				exception = "Header record failed to parse.";
			}
//			if (results == null || !results.getRecordName().equalsIgnoreCase("header")) {
//				crashNburn("Header does not match expected value for file: " + fileName, 1, fileLoadSummaryId, loadType, fileName, startTS);
//			}
		}
		Map<String, MessagePublisher> publishers = null;
		
		Boolean keepReading = true;
		List<Object> processResult = new ArrayList<Object>();
		Map<String, ProcessorDefinition> pi = new HashMap<String, ProcessorDefinition>();
		Integer totalLines = wc(file);
		List<Object> batchList = new ArrayList<Object>();
		Integer isFair;
		System.out.println("Target - " + totalLines);
		while (keepReading) {
			try {
				keepReading = ((results = ff.getNextRecord(bufIn)) != null);
				if (lineCount%(BATCH_SIZE*10)==0) {
					System.out.println("About " + ((float)Math.round((float)(lineCount)/(float)totalLines*10000))/100 + "% done. ");
				}
				if (results == null) {
					continue;
				}
				counts.incrementTotal();
				lineCount++;
			}
			catch (Exception e) {
				counts.incrementTotal();
				counts.incrementFailed();
				lineCount++;
				System.out.println("Caught: " + lineCount);
				addErrorToList(createError(fail, lineCount, "FAIL: " + e.getMessage(), fileLoadSummaryId));
				e.printStackTrace();
				continue;
			}
			finally {
				if ((isFair = checkFairRun()) != 0) {
					String message = "Load for file: " + fileName + " of type: " + loadType + System.getProperty("line.separator");
					message += "Many failed records: " + Math.round(((float)counts.getFailedRecords()/(float)(counts.getInsertedRecordStage()==0?counts.getTotalRecords():counts.getInsertedRecordStage())*10000)/100) + "%.";
					// Removing this as the minimal success records need to be loaded even if there are many failures
					// crashNburn(message, isFair, fileLoadSummaryId, loadType, fileName, startTS);
				}
			}
			if (linesToSkip > counts.getTotalRecords()) {
				continue;
			}
			if (linesToLoad < counts.getTotalRecords()) {
				keepReading = false;
			}
			if (counts.getTotalRecords() > BATCH_SIZE*2) {
				if ((float)counts.getFailedRecords()/(float)counts.getInsertedRecordStage()>.50) {
					String message = "Load for file: " + fileName + " of type: " + loadType + System.getProperty("line.separator");
					message += "Too many failed records: " + Math.round(((float)counts.getFailedRecords()/(float)(counts.getInsertedRecordStage())*10000)/100) + "%.";
					// Removing this as the minimal success records need to be loaded even if there are many failures
					// crashNburn(message, 1, fileLoadSummaryId, loadType, fileName, startTS);
				}else if ((float)counts.getFailedRecords()/(float)counts.getInsertedRecordStage()>.10) {
					String message = "Load for file: " + fileName + " of type: " + loadType + System.getProperty("line.separator");
					message += "Many failed records: " + Math.round(((float)counts.getFailedRecords()/(float)(counts.getInsertedRecordStage())*10000)/100) + "%.";
					// Removing this as the minimal success records need to be loaded even if there are many failures
					// crashNburn(message, 2, fileLoadSummaryId, loadType, fileName, startTS);
				}
			}

			try {
				// Added in order to ignore other records
				if(results.getRecordName().equalsIgnoreCase("other")) {
					continue;
				}
				if (results.getRecordName().equalsIgnoreCase("trailer") || results.getRecordName().equalsIgnoreCase("header")) {
					addErrorToList(createError(fail, lineCount, "FAIL: " + results.getRecordName().toUpperCase() + " RECORD", fileLoadSummaryId));
					continue;
				}
				String className = "com.futurerx.inbound." + companyName + "." + results.getRecordName();
				ProcessorDefinition p;
				if (pi.get(className) == null) {
					if (!classExists(className)) {
						className = "com.futurerx.inbound." + companyName + "." + results.getRecordName();
					}
					if (pi.get(className) == null) {
						Class<?> process = Class.forName(className);
						p = (ProcessorDefinition) process.newInstance();
						pi.put(className, p);
					}
					else {
						p = pi.get(className);
					}
				}
				else {
					p = pi.get(className);
				}
				processResult = p.process(results, fileLoadSummaryId, lineCount, companyName.toUpperCase());
				 System.out.println("Read Line: " + counts.getTotalRecords());

				if (validation) {
					if (validateData(fileLoadSummaryId, lineCount, processResult, codes, ep) == 0) {
						System.out.println("the number of records to process :: " + processResult.size());

						batchList.addAll(processResult);
					}
				}
				else {
					batchList.addAll(processResult);
				}
				if (batchList.size() >= BATCH_SIZE) {
					
					Callable<Counts> c = (Callable<Counts>) new ValidateNSaveThread(batchList);
					threadResults.add(pool.submit(c));						
					batchList = new ArrayList<Object>();
				}
				if (errors.size() >= BATCH_SIZE) {
					Callable<Counts> c = (Callable<Counts>) new ValidateNSaveThread(Arrays.asList(errors.toArray(new Object[0])));
					threadResults.add(pool.submit(c));						
					errors.clear();
				}
				while (threadResults.size() > threadPoolSize) {
					Thread.sleep(500);
					processResults();							
				}
			}
			catch (Exception e) {
				e.printStackTrace();
				addErrorToList(createError(fail, lineCount, e.toString(), fileLoadSummaryId));
				exception = e.getMessage();
				log.warn(exception);
			}
		}
		if (batchList.size() > 0) {
			Callable<Counts> c = (Callable<Counts>) new ValidateNSaveThread(batchList);
			threadResults.add(pool.submit(c));						
			batchList = new ArrayList<Object>();
		}
		if (errors.size() > 0) {
			Callable<Counts> c = (Callable<Counts>) new ValidateNSaveThread(Arrays.asList(errors.toArray(new Object[0])));
			threadResults.add(pool.submit(c));						
			errors.clear();
		}
		v.postprocess(ep);
		Counts cTemp = bs.save();
		incrememntCountsObject(cTemp);

		while (threadResults.size() > 0) {
			processResults();							
			Thread.sleep(500);
		}
		process(postprocessingSqlList);
		long endTime = (new Date()).getTime();
		System.out.println("Processing time: " + getElapsedTimeHoursMinutesSecondsString(endTime - startTime));
		System.out.println("Records processed: " + counts.getTotalRecords());
		System.out.println("Speed: " + counts.getTotalRecords()*1000/(endTime - startTime) + " recs/sec");
		finishProcessing(fileLoadSummaryId, loadType, fileName, startTS, exception);
		in.close();
		bufIn.close();
		getJdbcTemplate().getDataSource().getConnection().close();
	}
	
	private void crashNburn(String message, int status, Integer fileLoadSummaryId, String loadType, String fileName, Timestamp startTS) throws Exception {
		if (status == 2) {
			sendMail(message);
		}
		else if (status == 1) {
//			sentMail = false;
			sendMail(message + " Program Stopped.");
			finishProcessing(fileLoadSummaryId, loadType, fileName, startTS, message);
			System.exit(1);
			throw new Exception(message);
		}
	}

	private void sendMail(String message) {
		sendMail(null, message);
	}

	private void sendMail(String subject, String message) {
		if (!sentMail) {
			try {
				(new EmailAlert()).sendMessage(subject, message);
				sentMail = true;
			} catch (MessagingException e) {
				e.printStackTrace();
			}
		}
	}

	private void processResults() throws Exception {
		int cnt = 0;
		if (threadResults.size() > 0) {
			for (Iterator<Future<Counts>> it = threadResults.iterator();it.hasNext();) {
				Future<Counts> res = it.next();
				if (res.isDone()) {
					Counts c = res.get();
					incrememntCountsObject(c);
					it.remove();
					cnt++;
				}
			}
		}
		log.info("Processed " + cnt);
	}

	private void finishPreviousQueue(String queueName) throws Exception {
		//System.out.println("Finish Previous");
		MessageConsumer consumer = new ActivemqMessageConsumer();
		Map<String, String> config = new HashMap<String, String>();
		config.put("host", ActiveMQConnection.DEFAULT_BROKER_URL);
		config.put("wait", "0");
		try
		{
			//finishQueue = true;
			System.out.println("Clearing previous queue...");
			consumer.init(queueName, null, config);
			System.out.println("Done clearing previous queue.");
			//finishQueue = false;
		}
		catch(Exception e)
		{
			throw e;
		}
		finally
		{
			consumer.close();
		}
	}

	private void addErrorToList(FileLoadDetail fileLoadDetail) {
		if (fileLoadDetail != null) {
			errors.add(fileLoadDetail);
		}
	}
	
	private void incrememntCountsObject(Counts c) {
		if (c != null) {
			synchronized (counts) {
				counts.incrementSuccess(c.getInsertedRecordStage());
				counts.incrementFailed(c.getFailedRecords());
//				counts.incrementTotal(c.getTotalRecords());
			}
		}
	}
	private boolean classExists(String className) {
		try {
			Class.forName(className);
			return true;
		}
		catch (ClassNotFoundException exception) {
			return false;
		}
	}

	private Map<String, Map<String, Object>> loadMasterData()
	{
		Map<String, Map<String, Object>> retVal = new HashMap<String, Map<String, Object>>();
		int count = this.codeSets.size();
		for(int i = 0; i < count; i++)
		{
			Map<String, Object> code = this.codeSets.get(i);
			String codeType = (String)code.get("Code_type");
			String codeValue = (String)code.get("Code_value");
			if(codeType == null || codeValue == null)
			{
				continue;
			}
			Map<String, Object> codeMap = retVal.get(codeType);
			if(codeMap == null)
			{
				codeMap = new HashMap<String, Object>();
				codeMap.put(codeValue, code);
				retVal.put(codeType, codeMap);
			}
			else
			{
				codeMap.put(codeValue, code);
			}
		}
		return retVal;
	}

	public FileLoadDetail createError(String type, Long lineCount, String remark, Integer fileLoadSummaryId) {
		FileLoadDetail fld = new FileLoadDetail();
		fld.setFileLoadSummaryId(fileLoadSummaryId);
		fld.setRecordNumber(lineCount);
		fld.setRemark(remark);
		fld.setStatus((type.equalsIgnoreCase("FAIL")?1:2));
		return fld;
	}
	
	private Boolean checkPackage(String name) {
		Package[] lstPack = Package.getPackages();
		for (Package found : lstPack) {
			if (found.getName().equals(name)) {
				return true;
			}
			System.out.println(found.getName());
		}
		return false;
	}

	private Integer validateData(Integer fileLoadSummaryId, Long lineCount, List<Object> processResult, Map<String, Map<String, Object>> codes, ExpressionProcessor ep) {
		Integer recordsFailed = 0;
		Map<String, List<String>> validationResult = v.validate(processResult, codes, ep);
		
		if (validationResult.get(warn) != null) {
			String warning = "";
			for (String res : validationResult.get(warn)) {
				warning += (warn + ": " +res + "|");
			}
			warning = warning.substring(0, warning.length() - 1);
			addErrorToList(createError(warn, lineCount, warning, fileLoadSummaryId));
		}
		if (validationResult.get(fail) != null) {
			String failReason = "";
			for (String res : validationResult.get(fail)) {
				failReason += (fail + ": " + res + "|");
			}
			failReason = failReason.substring(0, failReason.length() - 1);
			addErrorToList(createError(fail, lineCount, failReason, fileLoadSummaryId));
			recordsFailed++;
			return 1;
		}
		return 0;
	}

	private void finishProcessing(Integer fileLoadSummaryId, String loadType, String filename, Timestamp startTS, String exception) {
		java.sql.Timestamp endTime = new java.sql.Timestamp(new Date().getTime());
		getJdbcTemplate().update(UPDATE_PROCESS_HISTORY_QUERY, endTime, counts.getTotalRecords(), counts.getInsertedRecordStage(), counts.getFailedRecords(), exception, "VNS Loader",startTS, filename);
		getJdbcTemplate().update(UPDATE_INTAKE_PROCESS_HISTORY_QUERY, endTime, 2, counts.getTotalRecords(), counts.getInsertedRecordStage(), counts.getFailedRecords(), fileLoadSummaryId);
	}
	
	private void finishValidationProcessing(Integer fileLoadSummaryId, Long records_failed, Long lastRecordProcessed) {
		java.sql.Timestamp endTime = new java.sql.Timestamp(new Date().getTime());
		getJdbcTemplate().update(UPDATE_INTAKE_PROCESS_VALIDATION_HISTORY_QUERY, endTime, 2, records_failed, lastRecordProcessed, fileLoadSummaryId);
	}

	private Map<String, Object> startProcessing(String filename, String loadType) {
		Timestamp startTS = new Timestamp(new Date().getTime());
		//filename = StringEscapeUtils.escapeSql(filename);
		getJdbcTemplate().update(INSERT_PROCESS_HISTORY_QUERY, companyName + " Loader", startTS, filename);
		getJdbcTemplate().update(INSERT_INTAKE_PROCESS_HISTORY_QUERY, filename, null, startTS, null, loadType, 0, null, null, 0);
		int fileLoadSummaryId = getJdbcTemplate().queryForInt(GET_FILE_LOAD_SUMMARY_ID, filename);
		Map<String, Object> retValue = new HashMap<String, Object>();
		retValue.put("id", new Integer(fileLoadSummaryId));
		retValue.put("time", startTS);
		return retValue;
	}
	
	/**
	 *  elapsed time in hours/minutes/seconds
	 * @return String
	 */
	public String getElapsedTimeHoursMinutesSecondsString(long elapsedTime) {     
		String format = String.format("%%0%dd", 2);
		elapsedTime = elapsedTime / 1000;
		String seconds = String.format(format, elapsedTime % 60);
		String minutes = String.format(format, (elapsedTime % 3600) / 60);
		String hours = String.format(format, elapsedTime / 3600);
		String time =  hours + ":" + minutes + ":" + seconds + "." + elapsedTime%1000;
		return time;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		Options opts = new Options();
		opts.addOption("help", false, "Prints this message");

		opts.addOption("notruncate", "no-truncate", false, "Truncate tables before starting load.");
		opts.addOption("commit", "batchsize", true, "Batch size");
		opts.addOption("threads", "thread-pool-size", true, "Number of threads");
		opts.addOption("fileLoadType", "loadType", true, "Types of files to be loaded separated by ';'.");
		opts.addOption("fileLoadPath", "loadPath", true, "Path of files to be loaded separated by ';'.");
		opts.addOption("cid", "companyId", true, "Trading partner Id or Company Id.");
		opts.addOption("format", "loadFormat", true, "Format of the files.");
		opts.addOption("nov", "no-validation", false, "Do not do any validation.");
		opts.addOption("lines", "countLines", true, "Number of lines to load.");
		opts.addOption("resume", "continue", true, "Continue if previous load did not finish.");
		opts.addOption("skip", "skipLines", true, "Skip these many lines.");
		opts.addOption("uq", "usequeue", false, "Use queue.");
		opts.addOption("qn", "queuename", true, "Queuename.");
		opts.addOption("mailOnly", "sendMail", true, "Send mail. Argument as Body");
		opts.addOption("noHeader", "noHeader", false, "No header in the file");
		opts.addOption("source", "sourceName", true, "Source name if a specific source needs to be defined.");

		Main m = new Main();
		CommandLine line = null;
		try {
			CommandLineParser parser = new GnuParser();			
			line = parser.parse(opts, args);
			if (line.hasOption("help")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp( "java -jar futurerx-loader.jar", opts);
				return;
			}
			if (line.hasOption("mailOnly")) {
				m.sendMail(line.getOptionValue("mailOnly"));
				System.exit(0);
			}
			if (line.hasOption("usequeue")) {
				usequeue = true;
			}
			if (line.hasOption("queuename")) {
				queueName = line.getOptionValue("queuename");
			}
			if (line.hasOption("sourceName")) {
				sourceName = line.getOptionValue("sourceName");
			}
			if (line.hasOption("notruncate")) {
				preprocess = false;
			}
			if (line.hasOption("lines")) {
				linesToLoad = Integer.parseInt(line.getOptionValue("lines"));
			}
			if (line.hasOption("commit")) {
				BATCH_SIZE = Integer.parseInt(line.getOptionValue("commit"));
			}
			if (line.hasOption("threads")) {
				threadPoolSize = Integer.parseInt(line.getOptionValue("threads"));
			}
			if (line.hasOption("nov")) {
				validation = false;
			}
			if (line.hasOption("noHeader")) {
				noHeader = true;
			}
			if (line.hasOption("skip")) {
				linesToSkip = Integer.parseInt(line.getOptionValue("skip"));
			}
			if (line.hasOption("resume")) {
				resume = true;
			}
			if (line.hasOption("cid")) {
				companyName = line.getOptionValue("companyId"); 
			}
			if (line.hasOption("format")) {
				format = line.getOptionValue("format"); //Check later to see if company name or ID. 
			}
			
			m.initialize();
			m.addShutdownHook();
			Validator.setPreprocess(preprocess);
			if(queueName == null)
			{
				if (line.hasOption("fileLoadType") && line.hasOption("fileLoadPath")) {
					String[] types = line.getOptionValue("fileLoadType").split(",");
					String[] paths = line.getOptionValue("fileLoadPath").split(",");
					if (types.length != paths.length) {
						throw new IllegalArgumentException("The count of paths should be equal to the count of types. Types = " + types.length + " Paths = " + paths.length + ".");
					}
					for (loop = 0; loop < types.length; loop++) {
						m.processFile(types[loop], paths[loop]);
					}
				}
			}
			
			System.out.println("Done with whatever needs to be done unless errored out. Exiting now.");
			m.finish();
		}
		catch (Exception e) {
			e.printStackTrace();
			m.finish();
			if (line!=null) { 
				Timestamp startTS =	(Timestamp)m.retValue.get("time");
				Integer fileLoadSummaryId =	(Integer)m.retValue.get("id");
				String[] types = line.getOptionValue("fileLoadType").split(",");
				String[] paths = line.getOptionValue("fileLoadPath").split(",");
			//	m.crashNburn(e.getMessage(), 1, fileLoadSummaryId, types[loop], paths[loop], startTS);
			}
			else {
				System.out.println("Exiting now. Unresolved issues not saved to database. ");
			}
		}
	}

	@Override
	public void init(Map<String, String> config) {
	}

	@Override
	public int handle(Object payload) {
		bs.incrementRecordsProcessed();
		Long lineNo = null;
		Integer fileLoadSummaryId = null;
		List<Object> data = null;
		try
		{
			RecordContainer recordContainer = (RecordContainer)payload;
			lineNo = recordContainer.getRecordNumber();
			fileLoadSummaryId = recordContainer.getFileLoadSummaryId();
			data = recordContainer.getData();
			System.out.println("Processing: " + recordContainer.getRecordNumber());
			if(fileLoadSummaryId == -1)
			{
				return MessageConsumer.STOP;
			}
			if (validation) {
				if (validateData(fileLoadSummaryId, lineNo, data, codes, ep) == 1) {
					Long errorCount = this.fileStats.get(String.valueOf(fileLoadSummaryId));
					if(errorCount == null)
					{
						errorCount = new Long(1);
						this.fileStats.put(String.valueOf(fileLoadSummaryId), errorCount);
					}
					else
					{
						errorCount = new Long(errorCount.longValue() + 1);
						this.fileStats.put(String.valueOf(fileLoadSummaryId), errorCount);
					}
				}
			}
			bs.addResults(data);
		//	bs.saveResults(data);
		}
		catch(Exception e)
		{
			bs.addError(fail, lineNo, e.getMessage(), fileLoadSummaryId);
			Long errorCount = this.fileStats.get(String.valueOf(fileLoadSummaryId));
			if(errorCount == null)
			{
				errorCount = new Long(1);
				this.fileStats.put(String.valueOf(fileLoadSummaryId), errorCount);
			}
			else
			{
				errorCount = new Long(errorCount.longValue() + 1);
				this.fileStats.put(String.valueOf(fileLoadSummaryId), errorCount);
			}
		}
		return MessageConsumer.PROCEED;
	}

	public int wc(File fileName) {
		int ls = 0;
		Scanner scanner;
		System.out.print(new Date() + ": Start scanning file...");
		try {
			scanner = new Scanner(fileName);

			while(scanner.hasNext())
			{
				scanner.nextLine();
				ls ++;
				if (ls%100000==0) {
					System.out.print(".");
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println("Done scanning file." + new Date());
		return ls;
	}
}
