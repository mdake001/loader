package com.futurerx.inbound.enterprise;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;

import com.blackbear.flatworm.MatchedRecord;
import com.futurerx.ProcessorDefinition;
import com.futurerx.stage.models.pharmacy.ProviderInformationLoad;

public class ProviderInformationProcessor implements ProcessorDefinition {
	@Override
	public List<Object> process(MatchedRecord results, Integer fileLoadSummaryId, Long lineCount, String companyCode) {
		List<Object> output = new ArrayList<Object>();
		ProviderInformationLoad pharm = (ProviderInformationLoad) results.getBean("provInfo");
		pharm.setRecordNumber(lineCount);
		pharm.setDataIntakeFileId(fileLoadSummaryId);
		pharm.setLoaderCompositeKey(generateProviderInformationLoadHash(pharm));
		output.add(pharm);
		return output;
	}
	
	private String generateProviderInformationLoadHash(ProviderInformationLoad p) {
		try {
			String hashInput = "";
			hashInput += StringUtils.upperCase(StringUtils.stripToEmpty(p.getNcpdpproviderid())) + "|";

			return DigestUtils.md5Hex(hashInput);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
