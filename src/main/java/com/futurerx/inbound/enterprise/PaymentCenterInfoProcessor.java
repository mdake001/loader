package com.futurerx.inbound.enterprise;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;

import com.blackbear.flatworm.MatchedRecord;
import com.futurerx.ProcessorDefinition;
import com.futurerx.stage.models.ProviderUPINLoad;
import com.futurerx.stage.models.pharmacy.ChangeOfOwnershipInformationLoad;
import com.futurerx.stage.models.pharmacy.PaymentCenterInformationLoad;

public class PaymentCenterInfoProcessor implements ProcessorDefinition {
	@Override
	public List<Object> process(MatchedRecord results, Integer fileLoadSummaryId, Long lineCount, String companyCode) {
		List<Object> output = new ArrayList<Object>();
		PaymentCenterInformationLoad pharm = (PaymentCenterInformationLoad) results.getBean("paymentCenter");
		pharm.setRecordNumber(lineCount);
		pharm.setDataIntakeFileId(fileLoadSummaryId);
		pharm.setLoaderCompositeKey(generatePaymentCenterInformationLoadHash(pharm));
		output.add(pharm);
		return output;
	}
	
	private String generatePaymentCenterInformationLoadHash(PaymentCenterInformationLoad p) {
		try {
			String hashInput = "";
			hashInput += StringUtils.upperCase(StringUtils.stripToEmpty(p.getPaymentCenterId())) + "|";

			return DigestUtils.md5Hex(hashInput);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
