package com.futurerx.inbound.enterprise;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;

import com.blackbear.flatworm.MatchedRecord;
import com.futurerx.ProcessorDefinition;
import com.futurerx.stage.models.pharmacy.StateMedicaidInformationLoad;

public class StateMedicaidInformationProcessor implements ProcessorDefinition {
	@Override
	public List<Object> process(MatchedRecord results, Integer fileLoadSummaryId, Long lineCount, String companyCode) {
		List<Object> output = new ArrayList<Object>();
		StateMedicaidInformationLoad pharm = (StateMedicaidInformationLoad) results.getBean("pharStateMedicaid");
		pharm.setRecordNumber(lineCount);
		pharm.setDataIntakeFileId(fileLoadSummaryId);
		pharm.setLoaderCompositeKey(generateStateMedicaidInformationLoadHash(pharm));
		output.add(pharm);
		return output;
	}
	
	private String generateStateMedicaidInformationLoadHash(StateMedicaidInformationLoad p) {
		try {
			String hashInput = "";
			hashInput += StringUtils.upperCase(StringUtils.stripToEmpty(p.getNcpdpproviderid())) + "|";

			return DigestUtils.md5Hex(hashInput);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
