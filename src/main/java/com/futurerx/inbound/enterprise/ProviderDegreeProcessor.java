package com.futurerx.inbound.enterprise;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;

import com.blackbear.flatworm.MatchedRecord;
import com.futurerx.ProcessorDefinition;
import com.futurerx.stage.models.ProviderDegreeLoad;

public class ProviderDegreeProcessor implements ProcessorDefinition {
	@Override
	public List<Object> process(MatchedRecord results, Integer fileLoadSummaryId, Long lineCount, String companyCode) {
		List<Object> output = new ArrayList<Object>();
		ProviderDegreeLoad prov = (ProviderDegreeLoad) results.getBean("provDegree");
		prov.setRecordNumber(lineCount);
		prov.setDataIntakeFileId(fileLoadSummaryId);
		prov.setLoaderCompositeKey(generateProviderDegreeHash(prov));
		output.add(prov);
		return output;
	}
	
	private String generateProviderDegreeHash(ProviderDegreeLoad p) {
		try {
			String hashInput = "";
			hashInput += StringUtils.upperCase(StringUtils.stripToEmpty(p.getHcid())) + "|";
			hashInput += StringUtils.upperCase(StringUtils.stripToEmpty(p.getDegree())) + "|";
			
			return DigestUtils.md5Hex(hashInput); 	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
