package com.futurerx.inbound.enterprise;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;

import com.blackbear.flatworm.MatchedRecord;
import com.futurerx.ProcessorDefinition;
import com.futurerx.stage.models.ProviderUPINLoad;
import com.futurerx.stage.models.pharmacy.FraudWasteAndAbuseTrainingAttestationLoad;

public class FraudWasteProcessor implements ProcessorDefinition {
	@Override
	public List<Object> process(MatchedRecord results, Integer fileLoadSummaryId, Long lineCount, String companyCode) {
		List<Object> output = new ArrayList<Object>();
		FraudWasteAndAbuseTrainingAttestationLoad pharm = (FraudWasteAndAbuseTrainingAttestationLoad) results.getBean("fraudWaste");
		pharm.setRecordNumber(lineCount);
		pharm.setDataIntakeFileId(fileLoadSummaryId);
		pharm.setLoaderCompositeKey(generateFraudWasteAndAbuseTrainingAttestationLoadHash(pharm));
		output.add(pharm);
		return output;
	}
	
	private String generateFraudWasteAndAbuseTrainingAttestationLoadHash(FraudWasteAndAbuseTrainingAttestationLoad p) {
		try {
			String hashInput = "";
			hashInput += StringUtils.upperCase(StringUtils.stripToEmpty(p.getNcpdpproviderid())) + "|";

			return DigestUtils.md5Hex(hashInput);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
