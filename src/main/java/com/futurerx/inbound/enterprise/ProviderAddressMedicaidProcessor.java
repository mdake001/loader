package com.futurerx.inbound.enterprise;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;

import com.blackbear.flatworm.MatchedRecord;
import com.futurerx.ProcessorDefinition;
import com.futurerx.stage.models.ProviderAddressLoad;
import com.futurerx.stage.models.ProviderAddressMedicaidLoad;

public class ProviderAddressMedicaidProcessor implements ProcessorDefinition {
	@Override
	public List<Object> process(MatchedRecord results, Integer fileLoadSummaryId, Long lineCount, String companyCode) {
		List<Object> output = new ArrayList<Object>();
		ProviderAddressMedicaidLoad prov = (ProviderAddressMedicaidLoad) results.getBean("provAddrMedicaid");
		prov.setRecordNumber(lineCount);
		prov.setDataIntakeFileId(fileLoadSummaryId);
		prov.setLoaderCompositeKey(generateProviderAddressMedicaidHash(prov));
		output.add(prov);
		return output;
	}

	private String generateProviderAddressMedicaidHash(ProviderAddressMedicaidLoad pam) {
		try {
			String hashInput = "";
			hashInput += StringUtils.upperCase(StringUtils.stripToEmpty(pam.getHcid())) + "|";
			hashInput += StringUtils.upperCase(StringUtils.stripToEmpty(pam.getAddressId())) + "|";
			hashInput += StringUtils.upperCase(StringUtils.stripToEmpty(pam.getState())) + "|";
			hashInput += StringUtils.upperCase(StringUtils.stripToEmpty(pam.getMedicaidId())) + "|";

			return DigestUtils.md5Hex(hashInput);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
