package com.futurerx.inbound.enterprise;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;

import com.blackbear.flatworm.MatchedRecord;
import com.futurerx.ProcessorDefinition;
import com.futurerx.stage.models.ProviderAddressPhoneLoad;

public class ProviderAddressPhoneProcessor implements ProcessorDefinition {
	@Override
	public List<Object> process(MatchedRecord results, Integer fileLoadSummaryId, Long lineCount, String companyCode) {
		List<Object> output = new ArrayList<Object>();
		ProviderAddressPhoneLoad prov = (ProviderAddressPhoneLoad) results.getBean("provAddrPhone");
		prov.setRecordNumber(lineCount);
		prov.setDataIntakeFileId(fileLoadSummaryId);
		prov.setLoaderCompositeKey(generateProviderAddressPhoneHash(prov));
		output.add(prov);
		return output;
	}

	private String generateProviderAddressPhoneHash(ProviderAddressPhoneLoad pap) {
		try {
			String hashInput = "";
			hashInput += StringUtils.upperCase(StringUtils.stripToEmpty(pap.getHcid())) + "|";
			hashInput += StringUtils.upperCase(StringUtils.stripToEmpty(pap.getAddressId())) + "|";
			hashInput += StringUtils.upperCase(StringUtils.stripToEmpty(pap.getPhoneType())) + "|";
			hashInput += StringUtils.upperCase(StringUtils.stripToEmpty(pap.getPhoneNumber())) + "|";

			return DigestUtils.md5Hex(hashInput);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
