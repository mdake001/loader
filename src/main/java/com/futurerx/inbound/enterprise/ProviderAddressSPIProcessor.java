package com.futurerx.inbound.enterprise;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;

import com.blackbear.flatworm.MatchedRecord;
import com.futurerx.ProcessorDefinition;
import com.futurerx.stage.models.ProviderAddressSPILoad;

public class ProviderAddressSPIProcessor implements ProcessorDefinition {
	@Override
	public List<Object> process(MatchedRecord results, Integer fileLoadSummaryId, Long lineCount, String companyCode) {
		List<Object> output = new ArrayList<Object>();
		ProviderAddressSPILoad prov = (ProviderAddressSPILoad) results.getBean("provAddSpi");
		prov.setRecordNumber(lineCount);
		prov.setDataIntakeFileId(fileLoadSummaryId);
		prov.setLoaderCompositeKey(generateProviderAddressSPIHash(prov));
		output.add(prov);
		return output;
	}
	
	private String generateProviderAddressSPIHash(ProviderAddressSPILoad p) {
		try {
			String hashInput = "";
			hashInput += StringUtils.upperCase(StringUtils.stripToEmpty(p.getHcid())) + "|";
			hashInput += StringUtils.upperCase(StringUtils.stripToEmpty(p.getAddressId())) + "|";
			hashInput += StringUtils.upperCase(StringUtils.stripToEmpty(p.getSpiRoot())) + "|";
			hashInput += StringUtils.upperCase(StringUtils.stripToEmpty(p.getSpiLocationCode())) + "|";
			
			return DigestUtils.md5Hex(hashInput); 
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
