package com.futurerx.inbound.enterprise;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;

import com.blackbear.flatworm.MatchedRecord;
import com.futurerx.ProcessorDefinition;
import com.futurerx.stage.models.ProviderStateLicenseLoad;

public class ProviderStateLicenseProcessor implements ProcessorDefinition {
	@Override
	public List<Object> process(MatchedRecord results, Integer fileLoadSummaryId, Long lineCount, String companyCode) {
		List<Object> output = new ArrayList<Object>();
		ProviderStateLicenseLoad prov = (ProviderStateLicenseLoad) results.getBean("provStateLicense");
		prov.setRecordNumber(lineCount);
		prov.setDataIntakeFileId(fileLoadSummaryId);
		prov.setLoaderCompositeKey(generateProviderStateLicenseHash(prov));
		output.add(prov);
		return output;
	}
	
	private String generateProviderStateLicenseHash(ProviderStateLicenseLoad p) {
		try {
			String hashInput = "";
			hashInput += StringUtils.upperCase(StringUtils.stripToEmpty(p.getHcid())) + "|";
			hashInput += StringUtils.upperCase(StringUtils.stripToEmpty(p.getLicenseNumber())) + "|";
			hashInput += StringUtils.upperCase(StringUtils.stripToEmpty(p.getLicenseState())) + "|";
			
			return DigestUtils.md5Hex(hashInput);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


}
