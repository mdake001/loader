package com.futurerx.inbound.enterprise;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;

import com.blackbear.flatworm.MatchedRecord;
import com.futurerx.ProcessorDefinition;
import com.futurerx.stage.models.ProviderNameLoad;

public class ProviderNameProcessor implements ProcessorDefinition {
	@Override
	public List<Object> process(MatchedRecord results, Integer fileLoadSummaryId, Long lineCount, String companyCode) {
		List<Object> output = new ArrayList<Object>();
		ProviderNameLoad prov = (ProviderNameLoad) results.getBean("provName");
		prov.setRecordNumber(lineCount);
		prov.setDataIntakeFileId(fileLoadSummaryId);
		prov.setLoaderCompositeKey(generateProviderNameHash(prov));
		output.add(prov);
		return output;
	}
	
	private String generateProviderNameHash(ProviderNameLoad p) {
		try {
			String hashInput = "";
			hashInput += StringUtils.upperCase(StringUtils.stripToEmpty(p.getHcid())) + "|";
			
			return DigestUtils.md5Hex(hashInput);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
