package com.futurerx.inbound.enterprise;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;

import com.blackbear.flatworm.MatchedRecord;
import com.futurerx.ProcessorDefinition;
import com.futurerx.stage.models.ProviderDeceasedDataLoad;

public class ProviderDeceasedDataProcessor implements ProcessorDefinition {
	@Override
	public List<Object> process(MatchedRecord results, Integer fileLoadSummaryId, Long lineCount, String companyCode) {
		List<Object> output = new ArrayList<Object>();
		ProviderDeceasedDataLoad prov = (ProviderDeceasedDataLoad) results.getBean("provDeceased");
		prov.setRecordNumber(lineCount);
		prov.setDataIntakeFileId(fileLoadSummaryId);
		prov.setLoaderCompositeKey(generateProviderDeceasedDataHash(prov));
		output.add(prov);
		return output;
	}
	
	private String generateProviderDeceasedDataHash(ProviderDeceasedDataLoad p) {
		try {
			String hashInput = "";
			hashInput += StringUtils.upperCase(StringUtils.stripToEmpty(p.getHcid())) + "|";
			hashInput += StringUtils.upperCase(StringUtils.stripToEmpty(p.getDeceasedIndicator())) + "|";
			
			return DigestUtils.md5Hex(hashInput); 
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


}
