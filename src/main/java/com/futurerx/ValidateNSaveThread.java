package com.futurerx;

import java.util.List;
import java.util.concurrent.Callable;

import com.futurerx.batch.BatchSave;
import com.futurerx.batch.Counts;

public class ValidateNSaveThread implements Callable<Counts>{
	private List<Object> processedRecords;
    private Class<BatchSave> batchSave = BatchSave.class;

	public ValidateNSaveThread(List<Object> processedRecords) {
		super();
		this.processedRecords = processedRecords;
	}
	
	public Counts call() throws Exception {
		Counts c = new Counts();
		try {
//			c.incrementTotal(processedRecords.size());
			BatchSave p = batchSave.newInstance();
			Counts cTemp = p.addResults(processedRecords);
			c.incrementFailed(cTemp.getFailedRecords());
			c.incrementSuccess(cTemp.getInsertedRecordStage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return c;
	}
}
