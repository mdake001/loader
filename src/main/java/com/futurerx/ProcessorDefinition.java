package com.futurerx;

import java.util.List;

import com.blackbear.flatworm.MatchedRecord;

public interface ProcessorDefinition {
	public List<Object> process(MatchedRecord results, Integer fileLoadSummaryId, Long lineCount, String companyCode);
}
