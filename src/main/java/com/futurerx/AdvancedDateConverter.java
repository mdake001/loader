package com.futurerx;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.blackbear.flatworm.Util;
import com.blackbear.flatworm.errors.FlatwormConversionException;


public class AdvancedDateConverter {
	private static Log log = LogFactory.getLog(AdvancedDateConverter.class);
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	public Date convertDate(String str, Map options) throws FlatwormConversionException {
        Date res = null;
		if (str.length() == 0 || str.length() < 8)
            return null;

		String format = (String) Util.getValue(options, "format");
        if (format == null) {
            format = "yyyyMMdd";
        } 
        try{
        	sdf = new SimpleDateFormat(format);
        	res = sdf.parse(str);
        }catch(Exception e){
        	e.printStackTrace();
        }
		return res;
    }
}
