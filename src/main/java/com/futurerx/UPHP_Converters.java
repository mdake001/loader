package com.futurerx;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.blackbear.flatworm.converters.CoreConverters;
import com.blackbear.flatworm.errors.FlatwormConversionException;

public class UPHP_Converters extends CoreConverters {
//	private static Log log = LogFactory.getLog(UPHP_Converters.class);

	private static Map<String,String> genderMap = null;

	public BigDecimal fixBigDecimal(String str,  Map<?, ?> options) {
		if (str == null || str.length() < 1 || str.length() > 9) {
			return null;
		}
		Float retVal = Float.parseFloat(str);
		return new BigDecimal(Float.toString(retVal));
	}
	static {
		genderMap = new HashMap<String, String>();
		genderMap.put("MALE", "Male");
		genderMap.put("FEMALE", "Female");
		genderMap.put("male", "Male");
		genderMap.put("female", "Female");
		genderMap.put("F", "Female");
		genderMap.put("M", "Male");
		genderMap.put("f", "Female");
		genderMap.put("m", "Male");
	}
	
	public Integer convertDecToInt(String str, Map<?, ?> options) throws FlatwormConversionException {
		double dblVal = Double.parseDouble(str);
		Integer intVal = (int) dblVal;
		return intVal;
	}
	
	public String convertGender(String str, Map<?, ?> options) throws FlatwormConversionException {
		String res = genderMap.get(str);
		return res;
    }
	
	public Boolean convertBoolean(String str,  Map<?, ?> options) {
		if (str != null && (str.startsWith("T") || str.startsWith("t") || str.startsWith("1"))) {
			return Boolean.TRUE;
		}
		else {
			return Boolean.FALSE;
		}
	}
}
