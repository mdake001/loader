package com.futurerx.batch;

public class Counts {
	private long totalRecords = 0;
    private long insertedRecords = 0;
    private long updatedRecords = 0;
    private long failedRecords = 0;
    private long insertedRecordStage = 0;
    private long failedRecordStage = 0;

    public long getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(long totalRecords) {
		this.totalRecords = totalRecords;
	}
	public long getInsertedRecords() {
		return insertedRecords;
	}
	public void setInsertedRecords(long successRecords) {
		this.insertedRecords = successRecords;
	}
	public long getFailedRecords() {
		return failedRecords;
	}
	public void setFailedRecords(long failedRecords) {
		this.failedRecords = failedRecords;
	}
    public void incrementSuccess() {
    	insertedRecords++;
    }
    public void incrementSuccess(long incCount) {
    	insertedRecords+=incCount;
    }
    public void incrementTotal() {
    	totalRecords++;
    }
    public void incrementTotal(long incCount) {
    	totalRecords+=incCount;
    }
    public void incrementFailed() {
    	failedRecords++;
    }
    public void incrementFailed(long incCount) {
    	failedRecords+=incCount;
    }
	public long getUpdatedRecords() {
		return updatedRecords;
	}
	public void setUpdatedRecords(long updatedRecords) {
		this.updatedRecords = updatedRecords;
	}
	public long getInsertedRecordStage() {
		return insertedRecordStage;
	}
	public void setInsertedRecordStage(long insertedRecordStage) {
		this.insertedRecordStage = insertedRecordStage;
	}
	public long getFailedRecordStage() {
		return failedRecordStage;
	}
	public void setFailedRecordStage(long failedRecordStage) {
		this.failedRecordStage = failedRecordStage;
	}
    
}
