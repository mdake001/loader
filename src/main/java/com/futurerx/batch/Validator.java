package com.futurerx.batch;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import com.futurerx.jep.Context;
import com.futurerx.jep.ExpressionProcessor;
import com.futurerx.validation.models.Action;
import com.futurerx.validation.models.Condition;
import com.futurerx.validation.models.PostprocessRule;
import com.futurerx.validation.models.PreprocessRule;
import com.futurerx.validation.models.Rule;
import com.futurerx.validation.models.RuleConfiguration;
import com.futurerx.validation.models.ValidationRule;

public class Validator {
	private static String fail = "FAIL", warn = "WARN";
	private static Boolean preprocess = true;
	private Map<String, RuleConfiguration> ruleConfigurationMap = new HashMap<String, RuleConfiguration>();

	public static void setPreprocess(Boolean preprocess) {
		Validator.preprocess = preprocess;
	}

	public Map<String, List<String>> validate(List<Object> lstResults, Map<String, Map<String, Object>> codes, ExpressionProcessor ep) {
		Map<String, List<String>> result = new HashMap<String, List<String>>();
		Map<String, Object> hmResult = new HashMap<String, Object>();
		for (Object obj: lstResults) {
			addToResultHM(hmResult, obj.getClass().getName(), obj);
		}
		result.putAll(getValidationListFromFile(hmResult, codes, ep));
		return result;
	}

	private void addToResultHM(Map<String, Object> hmResult, String name, Object obj) {
		Object currentObj = hmResult.get(name);
		if(currentObj == null){
			hmResult.put(name, obj);
		}
		else {
			if(currentObj instanceof List) {
				((List<Object>) currentObj).add(obj);
			}
			else {
				List<Object> currentObjList = new ArrayList<Object>();
				currentObjList.add(currentObj);
				currentObjList.add(obj);
				hmResult.put(name, currentObjList);
			}
		}
	}
	
	private Map<String, List<String>> getValidationListFromFile(Map<String, Object> validateObjects, Map<String, Map<String, Object>> codes, ExpressionProcessor ep) {
		Map<String, List<String>> result = new HashMap<String, List<String>>();
		List<String> originalKeyList = new ArrayList<String>(validateObjects.keySet());
		validateObjects.put("code", codes); 
		Properties props = null;
		ValidationRule evaluations = null;
		
		for (String key: originalKeyList) {
			String className = key.split("\\.")[key.split("\\.").length - 1];
			if (validateObjects.get(key) instanceof List) {
				for (Object objFromList : (List<Object>)validateObjects.get(key)) {
					Map<String, Object> hmObj = new HashMap<String, Object>();
					hmObj.put(objFromList.getClass().getName(), objFromList);
					result.putAll(getValidationListFromFile(hmObj, codes, ep));
				}
				ep.setContext(new Context(validateObjects));
				continue;
			}

			if (!ruleConfigurationMap.containsKey(className)) {
				try {
					String fileName = "PreLoadValidation"+ File.separator + className + ".validate.xml";
					JAXBContext jaxbContext = JAXBContext.newInstance(RuleConfiguration.class);
					Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
					RuleConfiguration vRule = (RuleConfiguration) jaxbUnmarshaller.unmarshal(new File(fileName));
					ruleConfigurationMap.put(className, vRule);
					if (preprocess)
						preprocess(vRule, ep);
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("Validator for " + className + " does not exist.");
					continue;
				}
			}
			evaluations = ruleConfigurationMap.get(className).getValidationRule();
			ep.setContext(new Context(validateObjects));
			
			for (Action a : evaluations.getAction()) {
				if (!a.getExpression().isEmpty()) {
					try {
						ep.evaluateExpression(a.getExpression());
					} catch (Exception e) {
						System.out.println("Error in action: " + a.getName());
						e.printStackTrace();
					}
				}
			}
			
			for (Rule r : evaluations.getRule()) {
				Condition c = r.getCondition();
				try {
					if (!ep.evaluateCondition(c.getExpression())) {
						if (!c.getFalse().isEmpty()) {
							if (result.get(c.getFalse().toUpperCase()) == null) {
								List<String> tempList = new ArrayList<String>();
								tempList.add(c.getName());
								result.put(c.getFalse().toUpperCase(), tempList);
							} else {
								result.get(c.getFalse().toUpperCase()).add(c.getName());
							}
						}
					}
					else {
						if (!c.getTrue().isEmpty()) {
							if (result.get(c.getTrue().toUpperCase()) == null) {
								List<String> tempList = new ArrayList<String>();
								tempList.add(c.getName());
								result.put(c.getTrue().toUpperCase(), tempList);
							} else {
								result.get(c.getTrue().toUpperCase()).add(c.getName());
							}
						}
					}
				} catch (Throwable e) {
					System.out.println("Error in condition: " + r.getName());
					e.printStackTrace();
				}
			}
		}
		return result;
	}

	private void preprocess(RuleConfiguration vRule, ExpressionProcessor ep) {
		PreprocessRule preRule = vRule.getPreprocessRule();
		if (preRule != null) {
			for (Action a : preRule.getAction()) {
				if (!a.getExpression().isEmpty()) {
					try {
						ep.evaluateExpression(a.getExpression());
					} catch (Exception e) {
						System.out.println("Error in preprocess action: "
								+ a.getName());
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	public void postprocess(ExpressionProcessor ep) {
		for (String key : ruleConfigurationMap.keySet()) {
			PostprocessRule postRule = ruleConfigurationMap.get(key).getPostprocessRule();
			if (postRule != null) {
				for (Action a : postRule.getAction()) {
					if (!a.getExpression().isEmpty()) {
						try {
							ep.evaluateExpression(a.getExpression());
						} catch (Exception e) {
							System.out.println("Error in postprocess action: "
									+ a.getName());
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
}
