package com.futurerx.batch;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.CharUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;

import com.futurerx.Main;
import com.futurerx.audit.models.FileLoadDetail;
import com.futurerx.batch.Counts;
import com.futurerx.stage.models.ProviderAddressLoad;
import com.futurerx.stage.models.ProviderAddressMedicaidLoad;
import com.futurerx.stage.models.ProviderAddressPhoneLoad;
import com.futurerx.stage.models.ProviderAddressSPILoad;
import com.futurerx.stage.models.ProviderBirthDateLoad;
import com.futurerx.stage.models.ProviderDEANumberDataWaiverLoad;
import com.futurerx.stage.models.ProviderDEANumberLoad;
import com.futurerx.stage.models.ProviderDeceasedDataLoad;
import com.futurerx.stage.models.ProviderDegreeLoad;
import com.futurerx.stage.models.ProviderGenderLoad;
import com.futurerx.stage.models.ProviderHCIDLoad;
import com.futurerx.stage.models.ProviderLoad;
import com.futurerx.stage.models.ProviderNPILoad;
import com.futurerx.stage.models.ProviderNameLoad;
import com.futurerx.stage.models.ProviderSpecialtyLoad;
import com.futurerx.stage.models.ProviderStateLicenseLoad;
import com.futurerx.stage.models.ProviderTaxonomyLoad;
import com.futurerx.stage.models.ProviderUPINLoad;
import com.futurerx.stage.models.pharmacy.ChangeOfOwnershipInformationLoad;
import com.futurerx.stage.models.pharmacy.EprescribingInformationLoad;
import com.futurerx.stage.models.pharmacy.FraudWasteAndAbuseTrainingAttestationLoad;
import com.futurerx.stage.models.pharmacy.ParentOrganizationInformationLoad;
import com.futurerx.stage.models.pharmacy.PaymentCenterInformationLoad;
import com.futurerx.stage.models.pharmacy.ProviderInformationLoad;
import com.futurerx.stage.models.pharmacy.ProviderRelationshipInformationLoad;
import com.futurerx.stage.models.pharmacy.RelationshipDemographicInformationLoad;
import com.futurerx.stage.models.pharmacy.RemitAndReconciliationInformationLoad;
import com.futurerx.stage.models.pharmacy.ServicesInformationLoad;
import com.futurerx.stage.models.pharmacy.StateLicenseInformationLoad;
import com.futurerx.stage.models.pharmacy.StateMedicaidInformationLoad;
import com.futurerx.stage.models.pharmacy.TaxonomyCodeInformationLoad;

public class BatchSave {

	private static String INSERT_PROVIDER_QUERY = "INSERT INTO futurerx_provider_stage.provider (hcid,companyCount,deltaDate,providerId,recordType,providerType,loaderCompositeKey,dataIntakeFileId, recordNumber ) "
			+ "VALUES (?,?,?,?,?,?,?,?,?)";

	private static String INSERT_PROVIDER_ADDRESS_QUERY = "INSERT INTO futurerx_provider_stage.provider_address (address_id,hcid,address1,address2,city,company_count"
			+ ",county,deltaDate,dpv,geoReturn,latitude,longitude,provider_id,addrRank,recordType,state,tierCode,verificationCode,verificationDate,zip,zip4,loaderCompositeKey, dataIntakeFileId, recordNumber) "
			+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	private static String INSERT_PROVIDER_ADDRESS_MEDICAID_QUERY = "INSERT INTO futurerx_provider_stage.provider_address_medicaid (address_id, hcid, medicaid_id, state, company_count, delta_date, provider_id, record_type, tier_code, verification_code, verification_date,"
			+ "loaderCompositeKey, dataIntakeFileId, recordNumber) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	private static String INSERT_PROVIDER_ADDRESS_PHONE_QUERY = "INSERT INTO futurerx_provider_stage.provider_address_phone (hcid,provider_id, address_id, phone_type,phone_number, company_count,tier_code,"
			+ "verification_code, verification_date, addrPhoneRank, record_type, delta_date, loaderCompositeKey, dataIntakeFileId, recordNumber) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	private static String INSERT_PROVIDER_ADDRESS_SPI_QUERY = "INSERT INTO futurerx_provider_stage.provider_address_spi (address_id,hcid,active_end_date,cancel,ccr,census,spi_change,company_count,delta_date,eligibility,med_history,spi_new,provider_id,re_supp,record_type,refill,rx_fill,service_level10,service_level11,service_level12,service_level13,service_level14,service_level15,service_level_code,spi_location_code,spi_root,tier_code,verification_code,verification_date,loaderCompositeKey, dataIntakeFileId, recordNumber) \r\n"
			+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	private static String INSERT_PROVIDER_DEA_NUMBER_QUERY = "INSERT INTO futurerx_provider_stage.provider_dea_number(dea_number,hcid,company_count,dea_status_code,delta_date,drug_schedule,provider_id,record_type,renewal_date,tier_code,verification_code,verification_date,loaderCompositeKey, dataIntakeFileId, recordNumber) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	private static String INSERT_PROVIDER_DEA_NUMBER_DATA_WAIVER_QUERY = "INSERT INTO futurerx_provider_stage.provider_dea_number_data_waiver(dea_number,hcid,activity_code,delta_date,provider_id,record_type,verification_date,loaderCompositeKey, dataIntakeFileId, recordNumber) VALUES(?,?,?,?,?,?,?,?,?,?)";
	
	private static String INSERT_PROVIDER_BIRTH_DATE_QUERY = "INSERT INTO futurerx_provider_stage.provider_birth_date(hcid,birth_date,company_count,delta_date,provider_id,record_type,tier_code,verification_code,verification_date,loaderCompositeKey, dataIntakeFileId, recordNumber)VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";

	private static String INSERT_PROVIDER_DECEASED_DATA_QUERY = "INSERT INTO futurerx_provider_stage.provider_deceased_data(deceased_indicator,hcid,deceased_date,delta_date,provider_id,record_type,loaderCompositeKey, dataIntakeFileId, recordNumber)VALUES(?,?,?,?,?,?,?,?,?)";
	
	private static String INSERT_PROVIDER_DEGREE_QUERY = "INSERT INTO futurerx_provider_stage.provider_degree(degree,hcid,company_count,delta_date,provider_id,record_type,tier_code,verification_date,verification_code,loaderCompositeKey, dataIntakeFileId, recordNumber)VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";

	private static String INSERT_PROVIDER_GENDER_QUERY = "INSERT INTO futurerx_provider_stage.provider_gender(hcid,company_count,delta_date,gender,provider_id,record_type,tier_code,verification_code,verification_date,loaderCompositeKey, dataIntakeFileId, recordNumber)VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";

	private static String INSERT_PROVIDER_HCID_QUERY = "INSERT INTO futurerx_provider_stage.provider_hcid(hcid,delta_date,provider_id,record_type,loaderCompositeKey, dataIntakeFileId, recordNumber)VALUES(?,?,?,?,?,?,?)";

	private static String INSERT_PROVIDER_NAME_QUERY = "INSERT INTO futurerx_provider_stage.provider_name(hcid,company_count,delta_date,first_name,last_name,middle_name,provider_id,record_type,suffix,tier_code,verification_code,verification_date,loaderCompositeKey, dataIntakeFileId, recordNumber)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	private static String INSERT_PROVIDER_NPI_QUERY = "INSERT INTO futurerx_provider_stage.provider_npi(hcid,company_count,deactivation_date,delta_date,npi,provider_id,npiRank,reactivation_date,record_type,tier_code,verification_code,verification_date,loaderCompositeKey, dataIntakeFileId, recordNumber)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	private static String INSERT_PROVIDER_SPECIALTY_QUERY = "INSERT INTO futurerx_provider_stage.provider_specialty(hcid,specialty_description,company_count,delta_date,provider_id,specRank,record_type,tier_code,verification_code,verification_date,loaderCompositeKey, dataIntakeFileId, recordNumber) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";

	private static String INSERT_PROVIDER_STATE_LICENSE_QUERY = "INSERT INTO futurerx_provider_stage.provider_state_license(hcid,license_number,license_state,company_count,delta_date,license_expiration_date,license_issue_date,provider_id,record_type,tier_code,verification_code,verification_date,loaderCompositeKey, dataIntakeFileId, recordNumber) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	private static String INSERT_PROVIDER_TAXONOMY_QUERY = "INSERT INTO futurerx_provider_stage.provider_taxonomy(hcid,taxonomy_code,company_count,delta_date,primary_taxonomy_indicator,provider_id,record_type,taxonomy_description,tier_code,verification_code,verification_date,loaderCompositeKey, dataIntakeFileId, recordNumber) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	private static String INSERT_PROVIDER_UPIN_QUERY = "INSERT INTO futurerx_provider_stage.provider_upin(hcid,upin,company_count,delta_date,provider_id,record_type,tier_code,verification_code,verification_date,loaderCompositeKey, dataIntakeFileId, recordNumber)VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
	
	private static String INSERT_CHANGE_OF_OWNER_QUERY = "INSERT INTO futurerx_pharmacy_stage.change_of_ownership_information(ncpdpproviderid,change_of_ownership_effective_date,filler,old_ncpdp_provider_id,old_store_close_date,dataIntakeFileId,recordNumber,loaderCompositeKey) VALUES (?,?,?,?,?,?,?,?)";
	
	private static String INSERT_EPRESCRIBING_QUERY = "INSERT INTO futurerx_pharmacy_stage.eprescribing_information(ncpdpproviderid,eprescribing_network_identifier,eprescribing_service_level_codes,effective_from_date,effective_through_date,filler,dataIntakeFileId,recordNumber,loaderCompositeKey) VALUES (?,?,?,?,?,?,?,?,?)";

	private static String INSERT_FRAUD_WASTE_QUERY = "INSERT INTO futurerx_pharmacy_stage.fraud_waste_and_abuse_training_attestation(ncpdpproviderid,accreditation_date,accreditation_organization,address1,address2,city,email,fax,filler,fwa_attestation,medicare_partd,npi,participating_pharmacy_or_psao_name,plan_year,q1,q2,q3,q4,responsible_party,signature_date,signature_of_responsible_party,state_code,version_number,zip_code,dataIntakeFileId,recordNumber,loaderCompositeKey) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	private static String INSERT_PARENT_ORG_QUERY = "INSERT INTO futurerx_pharmacy_stage.parent_organization_information(parent_organization_id,address1,address2,city,contact_name,contact_title,delete_date,e_mail_address,extension,fax_number,filler,parent_organization_federal_tax_id,parent_organization_name,parent_organization_npi,phone_number,state_code,zip_code,dataIntakeFileId,recordNumber,loaderCompositeKey) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	private static String INSERT_PAYMENT_CENTRE_QUERY = "INSERT INTO futurerx_pharmacy_stage.payment_center_information ( payment_center_id,delete_date,filler,payment_center_address1,payment_center_address2,payment_center_city,payment_center_contact_name,payment_center_contact_title,payment_centeremail_address,payment_center_extension,payment_center_fax_number,payment_center_federal_tax_id,payment_center_name,payment_center_npi,payment_center_phone_number,payment_center_state_code,payment_center_zip_code,dataIntakeFileId,recordNumber,loaderCompositeKey) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	private static String INSERT_PROVIDER_INFO_QUERY = "INSERT INTO futurerx_pharmacy_stage.provider_information(ncpdpproviderid,contact_email_address,contact_extension,contact_first_name,contact_last_name,contact_middle_initial,contact_phone_number,contact_title,deaexpiration_date,dearegistrationid,deactivation_code,dispenser_class_code,doctor_name,federal_taxidnumber,filler,legal_business_name,mailing_address1,mailing_address2,mailing_address_city,mailing_address_state_code,mailing_address_zip_code,medicare_provider_id,name,npi,physical_location24hour_operation_flag,physical_location_address1,physical_location_address2,physical_location_city,physical_location_congressional_voting_district,physical_location_country_parish,physical_location_cross_street,physical_location_email_address,physical_location_extension,physical_locationfax,physical_location_language_code1,physical_location_language_code2,physical_location_language_code3,physical_location_language_code4,physical_location_language_code5,physical_locationmsa,physical_locationpmsa,physical_location_phone_number,physical_location_provider_hours,physical_location_state_code,physical_location_store_closure_date,physical_location_store_open_date,physical_location_zip_code,primary_provider_type_code,reinstatement_code,reinstatement_date,secondary_provider_type_code,state_income_taxidnumber,store_number,tertiary_provider_type_code,transaction_code,transaction_date,dataIntakeFileId,recordNumber,loaderCompositeKey)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	private static String INSERT_PROV_REL_INFO_QUERY = "INSERT INTO futurerx_pharmacy_stage.provider_relationship_information(ncpdpproviderid,effective_from_date,effective_through_date,filler,is_primary,payment_center_id,provider_type,relationship_id,remit_and_reconciliation_id,dataIntakeFileId,recordNumber,loaderCompositeKey) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";

	private static String INSERT_REL_DEMO_QUERY = "INSERT INTO futurerx_pharmacy_stage.relationship_demographic_information(relationshipid,address1,address2,audit_contact_email,audit_contact_name,audit_contact_title,city,contact_name,contact_title,contractual_contact_email,contractual_contact_name,contractual_contact_title,delete_date,effective_from_date,email_address,extension,fax_number,filler,name,operational_contact_email,operational_contact_name,operational_contact_title,parent_organization_id,phone_number,relationship_federal_tax_id,relationshipnpi,relationship_type,state_code,technical_contact_email,technical_contact_name,technical_contact_title,zip_code,dataIntakeFileId,recordNumber,loaderCompositeKey)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	private static String INSERT_REMIT_RECON_QUERY = "INSERT INTO futurerx_pharmacy_stage.remit_and_reconciliation_information(remit_and_reconciliation_id,delete_date,filler,remit_and_reconciliation_address1,remit_and_reconciliation_address2,remit_and_reconciliation_city,remit_and_reconciliation_contact_name,remit_and_reconciliation_contact_title,remit_and_reconciliationemail_address,remit_and_reconciliation_extension,remit_and_reconciliation_fax_number,remit_and_reconciliation_federal_tax,remit_and_reconciliation_name,remit_and_reconciliation_npi,remit_and_reconciliation_phone_number,remit_and_reconciliation_state_code,remit_and_reconciliation_zip_code,dataIntakeFileId,recordNumber,loaderCompositeKey)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	private static String INSERT_SERVICE_INFO_QUERY = "INSERT INTO futurerx_pharmacy_stage.services_information(ncpdpproviderid,24hour_emergency_service_code,24hour_emergency_service_indicator,340b_status_code,340b_status_indicator,acceptseprescriptions_code,acceptseprescriptions_indicator,closed_door_facility_indicator,closed_door_facility_status_code,compounding_service_code,compounding_service_indicator,delivery_service_code,delivery_service_indicator,drive_up_window_code,drive_up_window_indicator,durable_medical_equipment_code,durable_medical_equipment_indicator,filler,handicapped_accessible_code,handicapped_accessible_indicator,immunizations_provided_code,immunizations_provided_indicator,multi_dose_compliance_packaging_code,multi_dose_compliance_packaging_indicator,walk_in_clinic_code,walk_in_clinic_indicator,dataIntakeFileId,recordNumber,loaderCompositeKey)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	private static String INSERT_STATE_LICENSE_QUERY = "INSERT INTO futurerx_pharmacy_stage.state_license_information(ncpdpproviderid,delete_date,filler,license_state_code,state_license_expiration_date,state_license_number,dataIntakeFileId,recordNumber,loaderCompositeKey)VALUES(?,?,?,?,?,?,?,?,?)";

	private static String INSERT_STATE_MEDICAID_QUERY = "INSERT INTO futurerx_pharmacy_stage.state_medicaid_information(ncpdpproviderid,delete_date,filler,medicaid_id,state_code,dataIntakeFileId,recordNumber,loaderCompositeKey) VALUES (?,?,?,?,?,?,?,?)";

	private static String INSERT_TAXO_CODE_INFO_QUERY = "INSERT INTO futurerx_pharmacy_stage.taxonomy_code_information(ncpdpproviderid,delete_date,filler,provider_type_code,taxonomy_code,dataIntakeFileId,recordNumber,loaderCompositeKey) VALUES (?,?,?,?,?,?,?,?)";

	private static String INSERT_INTAKE_ERROR_DETAIL = "INSERT INTO data_loading_audit.file_load_detail (file_load_summary_id, record_number, status, remark) VALUES (?, ?, ?, ?)";

	private static final ApplicationContext aContext = new ClassPathXmlApplicationContext("applicationContext.xml");
	private static final JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
	private Logger logger = LoggerFactory.getLogger(Main.class);
	// private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	private Integer BATCH_SIZE = 10;
	long startTime = (new Date()).getTime();
	long endTime = (new Date()).getTime();
	private Integer countFail = 0;

	List<FileLoadDetail> errors = new ArrayList<FileLoadDetail>();

	List<ProviderLoad> providers = new ArrayList<ProviderLoad>(BATCH_SIZE);
	List<ProviderAddressLoad> providerAddresses = new ArrayList<ProviderAddressLoad>(BATCH_SIZE);
	List<ProviderAddressMedicaidLoad> providerAddressesMedicaid = new ArrayList<ProviderAddressMedicaidLoad>(BATCH_SIZE);
	List<ProviderAddressPhoneLoad> providerAddressesPhone = new ArrayList<ProviderAddressPhoneLoad>(BATCH_SIZE);
	List<ProviderAddressSPILoad> providerAddressesSPI = new ArrayList<ProviderAddressSPILoad>(BATCH_SIZE);
	List<ProviderBirthDateLoad> providerBirthDate = new ArrayList<ProviderBirthDateLoad>(BATCH_SIZE);
	List<ProviderDEANumberLoad> providerDEANumber = new ArrayList<ProviderDEANumberLoad>(BATCH_SIZE);
	List<ProviderDEANumberDataWaiverLoad> providerDEANumberDataWaiver = new ArrayList<ProviderDEANumberDataWaiverLoad>(BATCH_SIZE);
	List<ProviderDeceasedDataLoad> providerDeceasedData = new ArrayList<ProviderDeceasedDataLoad>(BATCH_SIZE);
	List<ProviderDegreeLoad> providerDegree = new ArrayList<ProviderDegreeLoad>(BATCH_SIZE);
	List<ProviderGenderLoad> providerGender = new ArrayList<ProviderGenderLoad>(BATCH_SIZE);
	List<ProviderHCIDLoad> providerHCID = new ArrayList<ProviderHCIDLoad>(BATCH_SIZE);
	List<ProviderNameLoad> providerName = new ArrayList<ProviderNameLoad>(BATCH_SIZE);
	List<ProviderNPILoad> providerNPI = new ArrayList<ProviderNPILoad>(BATCH_SIZE);
	List<ProviderSpecialtyLoad> providerSpecialty = new ArrayList<ProviderSpecialtyLoad>(BATCH_SIZE);
	List<ProviderStateLicenseLoad> providerStateLicense = new ArrayList<ProviderStateLicenseLoad>(BATCH_SIZE);
	List<ProviderTaxonomyLoad> providerTaxonomy = new ArrayList<ProviderTaxonomyLoad>(BATCH_SIZE);
	List<ProviderUPINLoad> providerUPIN = new ArrayList<ProviderUPINLoad>(BATCH_SIZE);
	List<ChangeOfOwnershipInformationLoad> changeOfOwner = new ArrayList<ChangeOfOwnershipInformationLoad>(BATCH_SIZE);
	List<StateLicenseInformationLoad> stateLicense = new ArrayList<StateLicenseInformationLoad>(BATCH_SIZE);
	List<EprescribingInformationLoad> eprescribing = new ArrayList<EprescribingInformationLoad>(BATCH_SIZE);
	List<FraudWasteAndAbuseTrainingAttestationLoad> fraudWasteAndATA = new ArrayList<FraudWasteAndAbuseTrainingAttestationLoad>(BATCH_SIZE);
	List<ParentOrganizationInformationLoad> parentOrg = new ArrayList<ParentOrganizationInformationLoad>(BATCH_SIZE);
	List<PaymentCenterInformationLoad> paymentCentre = new ArrayList<PaymentCenterInformationLoad>(BATCH_SIZE);
	List<RelationshipDemographicInformationLoad> relDemographic = new ArrayList<RelationshipDemographicInformationLoad>(BATCH_SIZE);
	List<RemitAndReconciliationInformationLoad> remitAndReconciliation = new ArrayList<RemitAndReconciliationInformationLoad>(BATCH_SIZE);
	List<ServicesInformationLoad> serviceInfo = new ArrayList<ServicesInformationLoad>(BATCH_SIZE);
	List<StateMedicaidInformationLoad> stateMedicaid = new ArrayList<StateMedicaidInformationLoad>(BATCH_SIZE);
	List<TaxonomyCodeInformationLoad> taxonomyCode = new ArrayList<TaxonomyCodeInformationLoad>(BATCH_SIZE);
	List<ProviderInformationLoad> providerInfo = new ArrayList<ProviderInformationLoad>(BATCH_SIZE);
	List<ProviderRelationshipInformationLoad> providerRelInfo = new ArrayList<ProviderRelationshipInformationLoad>(BATCH_SIZE);


	int recordsProcessed = 0;

	private JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public ApplicationContext getaContext() {
		return aContext;
	}

	public BatchSave() {
		super();
	}

	public BatchSave(JdbcTemplate jdbcTemplate, ApplicationContext aContext, Integer BATCH_SIZE) {
		super();
		this.BATCH_SIZE = BATCH_SIZE;
	}

	public Counts addResults(List<Object> result) {
		for (Object obj : result) {
			System.out.println("Class name in the record :: " + obj.getClass().getName().toString());
			if (obj.getClass().getName().contains("ProviderLoad")) {
				providers.add((ProviderLoad) obj);
			} else if (obj.getClass().getName().contains("ProviderAddressLoad")) {
				providerAddresses.add((ProviderAddressLoad) obj);
			} else if (obj.getClass().getName().contains("ProviderAddressMedicaidLoad")) {
				providerAddressesMedicaid.add((ProviderAddressMedicaidLoad) obj);
			} else if (obj.getClass().getName().contains("ProviderAddressPhoneLoad")) {
				providerAddressesPhone.add((ProviderAddressPhoneLoad) obj);
			} else if (obj.getClass().getName().contains("ProviderAddressSPILoad")) {
				providerAddressesSPI.add((ProviderAddressSPILoad) obj);
			} else if (obj.getClass().getName().contains("ProviderBirthDateLoad")) {
				providerBirthDate.add((ProviderBirthDateLoad) obj);
			} else if (obj.getClass().getName().contains("ProviderDEANumberLoad")) {
				providerDEANumber.add((ProviderDEANumberLoad) obj);
			} else if (obj.getClass().getName().contains("ProviderDEANumberDataWaiverLoad")) {
				providerDEANumberDataWaiver.add((ProviderDEANumberDataWaiverLoad) obj);
			} else if (obj.getClass().getName().contains("ProviderDeceasedDataLoad")) {
				providerDeceasedData.add((ProviderDeceasedDataLoad) obj);
			} else if (obj.getClass().getName().contains("ProviderDegreeLoad")) {
				providerDegree.add((ProviderDegreeLoad) obj);
			} else if (obj.getClass().getName().contains("ProviderGenderLoad")) {
				providerGender.add((ProviderGenderLoad) obj);
			} else if (obj.getClass().getName().contains("ProviderHCIDLoad")) {
				providerHCID.add((ProviderHCIDLoad) obj);
			} else if (obj.getClass().getName().contains("ProviderNameLoad")) {
				providerName.add((ProviderNameLoad) obj);
			} else if (obj.getClass().getName().contains("ProviderNPILoad")) {
				providerNPI.add((ProviderNPILoad) obj);
			} else if (obj.getClass().getName().contains("ProviderSpecialtyLoad")) {
				providerSpecialty.add((ProviderSpecialtyLoad) obj);
			} else if (obj.getClass().getName().contains("ProviderStateLicenseLoad")) {
				providerStateLicense.add((ProviderStateLicenseLoad) obj);
			} else if (obj.getClass().getName().contains("ProviderTaxonomyLoad")) {
				providerTaxonomy.add((ProviderTaxonomyLoad) obj);
			} else if (obj.getClass().getName().contains("ProviderUPINLoad")) {
				providerUPIN.add((ProviderUPINLoad) obj);
			} else if (obj.getClass().getName().contains("ChangeOfOwnershipInformationLoad")) {
				changeOfOwner.add((ChangeOfOwnershipInformationLoad) obj);
			} else if (obj.getClass().getName().contains("EprescribingInformationLoad")) {
				eprescribing.add((EprescribingInformationLoad) obj);
			} else if (obj.getClass().getName().contains("FraudWasteAndAbuseTrainingAttestationLoad")) {
				fraudWasteAndATA.add((FraudWasteAndAbuseTrainingAttestationLoad) obj);
			} else if (obj.getClass().getName().contains("ParentOrganizationInformationLoad")) {
				parentOrg.add((ParentOrganizationInformationLoad) obj);
			} else if (obj.getClass().getName().contains("PaymentCenterInformationLoad")) {
				paymentCentre.add((PaymentCenterInformationLoad) obj);
			} else if (obj.getClass().getName().contains("ProviderInformationLoad")) {
				providerInfo.add((ProviderInformationLoad) obj);
			} else if (obj.getClass().getName().contains("ProviderRelationshipInformationLoad")) {
				providerRelInfo.add((ProviderRelationshipInformationLoad) obj);
			} else if (obj.getClass().getName().contains("RelationshipDemographicInformationLoad")) {
				relDemographic.add((RelationshipDemographicInformationLoad) obj);
			} else if (obj.getClass().getName().contains("RemitAndReconciliationInformationLoad")) {
				remitAndReconciliation.add((RemitAndReconciliationInformationLoad) obj);
			} else if (obj.getClass().getName().contains("ServicesInformationLoad")) {
				serviceInfo.add((ServicesInformationLoad) obj);
			} else if (obj.getClass().getName().contains("StateLicenseInformationLoad")) {
				stateLicense.add((StateLicenseInformationLoad) obj);
			} else if (obj.getClass().getName().contains("StateMedicaidInformationLoad")) {
				stateMedicaid.add((StateMedicaidInformationLoad) obj);
			} else if (obj.getClass().getName().contains("TaxonomyCodeInformationLoad")) {
				taxonomyCode.add((TaxonomyCodeInformationLoad) obj);
			} 
		}

		return save();
	}

	public void incrementRecordsProcessed() {
		this.recordsProcessed++;
	}

	public String getElapsedTimeHoursMinutesSecondsString(long elapsedTime) {
		String format = String.format("%%0%dd", 2);
		elapsedTime = elapsedTime / 1000;
		String seconds = String.format(format, elapsedTime % 60);
		String minutes = String.format(format, (elapsedTime % 3600) / 60);
		String hours = String.format(format, elapsedTime / 3600);
		String time = hours + ":" + minutes + ":" + seconds + "." + elapsedTime % 1000;
		return time;
	}

	public Counts save() {

		Counts c = new Counts();
		Integer recordsProcessed = 0;

		try {
			if (providers.size() > 0) {
				batchProcessProviders(providers);
				recordsProcessed += providers.size();
				providers.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, providers.get(0).getDataIntakeFileId(), providers.get(0).getRecordNumber(),
					providers.size(), providers.get(0).getClass().getName());
			c.incrementFailed(providers.size() - 1);
			providers.clear();
			e.printStackTrace();
		}
		try {
			if (providerAddresses.size() > 0) {
				batchProcessProviderAddresses(providerAddresses);
				recordsProcessed += providerAddresses.size();
				providerAddresses.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, providerAddresses.get(0).getDataIntakeFileId(),
					providerAddresses.get(0).getRecordNumber(), providerAddresses.size(),
					providerAddresses.get(0).getClass().getName());
			c.incrementFailed(providerAddresses.size() - 1);
			providerAddresses.clear();
			e.printStackTrace();
		}
		try {
			if (providerAddressesMedicaid.size() > 0) {
				batchProcessProviderAddressMedicaid(providerAddressesMedicaid);
				recordsProcessed += providerAddressesMedicaid.size();
				providerAddressesMedicaid.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, providerAddressesMedicaid.get(0).getDataIntakeFileId(),
					providerAddressesMedicaid.get(0).getRecordNumber(), providerAddressesMedicaid.size(),
					providerAddressesMedicaid.get(0).getClass().getName());
			c.incrementFailed(providerAddressesMedicaid.size() - 1);
			providerAddressesMedicaid.clear();
			e.printStackTrace();
		}
		try {
			if (providerAddressesPhone.size() > 0) {
				batchProcessProviderAddressPhone(providerAddressesPhone);
				recordsProcessed += providerAddressesPhone.size();
				providerAddressesPhone.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, providerAddressesPhone.get(0).getDataIntakeFileId(),
					providerAddressesPhone.get(0).getRecordNumber(), providerAddressesPhone.size(),
					providerAddressesPhone.get(0).getClass().getName());
			c.incrementFailed(providerAddressesPhone.size() - 1);
			providerAddressesPhone.clear();
			e.printStackTrace();
		}
		try {
			if (providerAddressesSPI.size() > 0) {
				batchProcessProviderAddressSPI(providerAddressesSPI);
				recordsProcessed += providerAddressesSPI.size();
				providerAddressesSPI.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, providerAddressesSPI.get(0).getDataIntakeFileId(),
					providerAddressesSPI.get(0).getRecordNumber(), providerAddressesSPI.size(),
					providerAddressesSPI.get(0).getClass().getName());
			c.incrementFailed(providerAddressesSPI.size() - 1);
			providerAddressesSPI.clear();
			e.printStackTrace();
		}
		try {
			if (providerBirthDate.size() > 0) {
				batchProcessProviderBirthDate(providerBirthDate);
				recordsProcessed += providerBirthDate.size();
				providerBirthDate.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, providerBirthDate.get(0).getDataIntakeFileId(),
					providerBirthDate.get(0).getRecordNumber(), providerBirthDate.size(),
					providerBirthDate.get(0).getClass().getName());
			c.incrementFailed(providerBirthDate.size() - 1);
			providerBirthDate.clear();
			e.printStackTrace();
		}
		try {
			if (providerDEANumber.size() > 0) {
				batchProcessProviderDEANumber(providerDEANumber);
				recordsProcessed += providerDEANumber.size();
				providerDEANumber.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, providerDEANumber.get(0).getDataIntakeFileId(),
					providerDEANumber.get(0).getRecordNumber(), providerDEANumber.size(),
					providerDEANumber.get(0).getClass().getName());
			c.incrementFailed(providerDEANumber.size() - 1);
			providerDEANumber.clear();
			e.printStackTrace();
		}try {
			if (providerDEANumberDataWaiver.size() > 0) {
				batchProcessProviderDEANumberDataWaiver(providerDEANumberDataWaiver);
				recordsProcessed += providerDEANumberDataWaiver.size();
				providerDEANumberDataWaiver.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, providerDEANumberDataWaiver.get(0).getDataIntakeFileId(),
					providerDEANumberDataWaiver.get(0).getRecordNumber(), providerDEANumberDataWaiver.size(),
					providerDEANumberDataWaiver.get(0).getClass().getName());
			c.incrementFailed(providerDEANumberDataWaiver.size() - 1);
			providerDEANumberDataWaiver.clear();
			e.printStackTrace();
		}
		try {
			if (providerDeceasedData.size() > 0) {
				batchProcessProviderDeceasedData(providerDeceasedData);
				recordsProcessed += providerDeceasedData.size();
				providerDeceasedData.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, providerDeceasedData.get(0).getDataIntakeFileId(),
					providerDeceasedData.get(0).getRecordNumber(), providerDeceasedData.size(),
					providerDeceasedData.get(0).getClass().getName());
			c.incrementFailed(providerDeceasedData.size() - 1);
			providerDeceasedData.clear();
			e.printStackTrace();
		}
		try {
			if (providerDegree.size() > 0) {
				batchProcessProviderDegree(providerDegree);
				recordsProcessed += providerDegree.size();
				providerDegree.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, providerDegree.get(0).getDataIntakeFileId(),
					providerDegree.get(0).getRecordNumber(), providerDegree.size(),
					providerDegree.get(0).getClass().getName());
			c.incrementFailed(providerDegree.size() - 1);
			providerDegree.clear();
			e.printStackTrace();
		}try {
			if (providerGender.size() > 0) {
				batchProcessProviderGender(providerGender);
				recordsProcessed += providerGender.size();
				providerGender.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, providerGender.get(0).getDataIntakeFileId(),
					providerGender.get(0).getRecordNumber(), providerGender.size(),
					providerGender.get(0).getClass().getName());
			c.incrementFailed(providerGender.size() - 1);
			providerGender.clear();
			e.printStackTrace();
		}
		try {
			if (providerHCID.size() > 0) {
				batchProcessProviderHCID(providerHCID);
				recordsProcessed += providerHCID.size();
				providerHCID.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, providerHCID.get(0).getDataIntakeFileId(),
					providerHCID.get(0).getRecordNumber(), providerHCID.size(),
					providerHCID.get(0).getClass().getName());
			c.incrementFailed(providerHCID.size() - 1);
			providerHCID.clear();
			e.printStackTrace();
		}
		try {
			if (providerName.size() > 0) {
				batchProcessProviderName(providerName);
				recordsProcessed += providerName.size();
				providerName.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, providerName.get(0).getDataIntakeFileId(),
					providerName.get(0).getRecordNumber(), providerName.size(),
					providerName.get(0).getClass().getName());
			c.incrementFailed(providerName.size() - 1);
			providerName.clear();
			e.printStackTrace();
		}try {
			if (providerNPI.size() > 0) {
				batchProcessProviderNPI(providerNPI);
				recordsProcessed += providerNPI.size();
				providerNPI.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, providerNPI.get(0).getDataIntakeFileId(),
					providerNPI.get(0).getRecordNumber(), providerNPI.size(),
					providerNPI.get(0).getClass().getName());
			c.incrementFailed(providerNPI.size() - 1);
			providerNPI.clear();
			e.printStackTrace();
		}try {
			if (providerSpecialty.size() > 0) {
				batchProcessProviderSpecialty(providerSpecialty);
				recordsProcessed += providerSpecialty.size();
				providerSpecialty.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, providerSpecialty.get(0).getDataIntakeFileId(),
					providerSpecialty.get(0).getRecordNumber(), providerSpecialty.size(),
					providerSpecialty.get(0).getClass().getName());
			c.incrementFailed(providerSpecialty.size() - 1);
			providerSpecialty.clear();
			e.printStackTrace();
		}
		try {
			if (providerStateLicense.size() > 0) {
				batchProcessProviderStateLicense(providerStateLicense);
				recordsProcessed += providerStateLicense.size();
				providerStateLicense.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, providerStateLicense.get(0).getDataIntakeFileId(),
					providerStateLicense.get(0).getRecordNumber(), providerStateLicense.size(),
					providerStateLicense.get(0).getClass().getName());
			c.incrementFailed(providerStateLicense.size() - 1);
			providerStateLicense.clear();
			e.printStackTrace();
		}try {
			if (providerTaxonomy.size() > 0) {
				batchProcessProviderTaxonomy(providerTaxonomy);
				recordsProcessed += providerTaxonomy.size();
				providerTaxonomy.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, providerTaxonomy.get(0).getDataIntakeFileId(),
					providerTaxonomy.get(0).getRecordNumber(), providerTaxonomy.size(),
					providerTaxonomy.get(0).getClass().getName());
			c.incrementFailed(providerTaxonomy.size() - 1);
			providerTaxonomy.clear();
			e.printStackTrace();
		}
		try {
			if (providerUPIN.size() > 0) {
				batchProcessProviderUPIN(providerUPIN);
				recordsProcessed += providerUPIN.size();
				providerUPIN.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, providerUPIN.get(0).getDataIntakeFileId(),
					providerUPIN.get(0).getRecordNumber(), providerUPIN.size(),
					providerUPIN.get(0).getClass().getName());
			c.incrementFailed(providerUPIN.size() - 1);
			providerUPIN.clear();
			e.printStackTrace();
		}
		try {
			if (changeOfOwner.size() > 0) {
				batchProcessChangeOfOwner(changeOfOwner);
				recordsProcessed += changeOfOwner.size();
				changeOfOwner.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, changeOfOwner.get(0).getDataIntakeFileId(),
					changeOfOwner.get(0).getRecordNumber(), changeOfOwner.size(),
					changeOfOwner.get(0).getClass().getName());
			c.incrementFailed(changeOfOwner.size() - 1);
			changeOfOwner.clear();
			e.printStackTrace();
		}
		try {
			if (eprescribing.size() > 0) {
				batchProcessEprescribing(eprescribing);
				recordsProcessed += eprescribing.size();
				eprescribing.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, eprescribing.get(0).getDataIntakeFileId(),
					eprescribing.get(0).getRecordNumber(), eprescribing.size(),
					eprescribing.get(0).getClass().getName());
			c.incrementFailed(eprescribing.size() - 1);
			eprescribing.clear();
			e.printStackTrace();
		}
		try {
			if (fraudWasteAndATA.size() > 0) {
				batchProcessFraudWasteAndATA(fraudWasteAndATA);
				recordsProcessed += fraudWasteAndATA.size();
				fraudWasteAndATA.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, fraudWasteAndATA.get(0).getDataIntakeFileId(),
					fraudWasteAndATA.get(0).getRecordNumber(), fraudWasteAndATA.size(),
					fraudWasteAndATA.get(0).getClass().getName());
			c.incrementFailed(fraudWasteAndATA.size() - 1);
			fraudWasteAndATA.clear();
			e.printStackTrace();
		}
		try {
			if (parentOrg.size() > 0) {
				batchProcessParentOrg(parentOrg);
				recordsProcessed += parentOrg.size();
				parentOrg.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, parentOrg.get(0).getDataIntakeFileId(),
					parentOrg.get(0).getRecordNumber(), parentOrg.size(),
					parentOrg.get(0).getClass().getName());
			c.incrementFailed(parentOrg.size() - 1);
			parentOrg.clear();
			e.printStackTrace();
		}
		try {
			if (paymentCentre.size() > 0) {
				batchProcessPaymentCentre(paymentCentre);
				recordsProcessed += paymentCentre.size();
				paymentCentre.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, paymentCentre.get(0).getDataIntakeFileId(),
					paymentCentre.get(0).getRecordNumber(), paymentCentre.size(),
					paymentCentre.get(0).getClass().getName());
			c.incrementFailed(paymentCentre.size() - 1);
			paymentCentre.clear();
			e.printStackTrace();
		}
		try {
			if (providerInfo.size() > 0) {
				batchProcessProviderInfo(providerInfo);
				recordsProcessed += providerInfo.size();
				providerInfo.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, providerInfo.get(0).getDataIntakeFileId(),
					providerInfo.get(0).getRecordNumber(), providerInfo.size(),
					providerInfo.get(0).getClass().getName());
			c.incrementFailed(providerInfo.size() - 1);
			providerInfo.clear();
			e.printStackTrace();
		}
		try {
			if (providerRelInfo.size() > 0) {
				batchProcessProviderRelInfo(providerRelInfo);
				recordsProcessed += providerRelInfo.size();
				providerRelInfo.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, providerRelInfo.get(0).getDataIntakeFileId(),
					providerRelInfo.get(0).getRecordNumber(), providerRelInfo.size(),
					providerRelInfo.get(0).getClass().getName());
			c.incrementFailed(providerRelInfo.size() - 1);
			providerRelInfo.clear();
			e.printStackTrace();
		}
		try {
			if (relDemographic.size() > 0) {
				batchProcessRelDemographic(relDemographic);
				recordsProcessed += relDemographic.size();
				relDemographic.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, relDemographic.get(0).getDataIntakeFileId(),
					relDemographic.get(0).getRecordNumber(), relDemographic.size(),
					relDemographic.get(0).getClass().getName());
			c.incrementFailed(relDemographic.size() - 1);
			relDemographic.clear();
			e.printStackTrace();
		}
		try {
			if (remitAndReconciliation.size() > 0) {
				batchProcessRemitAndReconciliation(remitAndReconciliation);
				recordsProcessed += remitAndReconciliation.size();
				remitAndReconciliation.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, remitAndReconciliation.get(0).getDataIntakeFileId(),
					remitAndReconciliation.get(0).getRecordNumber(), remitAndReconciliation.size(),
					remitAndReconciliation.get(0).getClass().getName());
			c.incrementFailed(remitAndReconciliation.size() - 1);
			remitAndReconciliation.clear();
			e.printStackTrace();
		}
		try {
			if (serviceInfo.size() > 0) {
				batchProcessServiceInfo(serviceInfo);
				recordsProcessed += serviceInfo.size();
				serviceInfo.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, serviceInfo.get(0).getDataIntakeFileId(),
					serviceInfo.get(0).getRecordNumber(), serviceInfo.size(),
					serviceInfo.get(0).getClass().getName());
			c.incrementFailed(serviceInfo.size() - 1);
			serviceInfo.clear();
			e.printStackTrace();
		}
		try {
			if (stateLicense.size() > 0) {
				batchProcessStateLicense(stateLicense);
				recordsProcessed += stateLicense.size();
				stateLicense.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, stateLicense.get(0).getDataIntakeFileId(),
					stateLicense.get(0).getRecordNumber(), stateLicense.size(),
					stateLicense.get(0).getClass().getName());
			c.incrementFailed(stateLicense.size() - 1);
			stateLicense.clear();
			e.printStackTrace();
		}
		try {
			if (stateMedicaid.size() > 0) {
				batchProcessStateMedicaid(stateMedicaid);
				recordsProcessed += stateMedicaid.size();
				stateMedicaid.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, stateMedicaid.get(0).getDataIntakeFileId(),
					stateMedicaid.get(0).getRecordNumber(), stateMedicaid.size(),
					stateMedicaid.get(0).getClass().getName());
			c.incrementFailed(stateMedicaid.size() - 1);
			stateMedicaid.clear();
			e.printStackTrace();
		}
		try {
			if (taxonomyCode.size() > 0) {
				batchProcessTaxonomyCode(taxonomyCode);
				recordsProcessed += taxonomyCode.size();
				taxonomyCode.clear();
			}
		} catch (Exception e) {
			saveBatchError(e, taxonomyCode.get(0).getDataIntakeFileId(),
					taxonomyCode.get(0).getRecordNumber(), taxonomyCode.size(),
					taxonomyCode.get(0).getClass().getName());
			c.incrementFailed(taxonomyCode.size() - 1);
			taxonomyCode.clear();
			e.printStackTrace();
		}
		
		c.setInsertedRecordStage(recordsProcessed);
		return c;
	}

	private void saveBatchError(Exception e, Integer dataIntakeFileId, long minId, Integer size, String type) {
		FileLoadDetail fld = new FileLoadDetail();
		fld.setFileLoadSummaryId(dataIntakeFileId);
		fld.setRecordNumber(minId);
		fld.setRemark("FAIL: Batch error <SIZE:" + size + "> Message: " + e.getMessage());
		fld.setStatus(1);
		errors.add(fld);
	}

	public Counts addError(String type, Long lineCount, String remark, Integer fileLoadSummaryId) {
		FileLoadDetail fld = new FileLoadDetail();
		fld.setFileLoadSummaryId(fileLoadSummaryId);
		fld.setRecordNumber(lineCount);
		fld.setRemark(remark);
		fld.setStatus((type.equalsIgnoreCase("FAIL") ? 1 : 2));
		errors.add(fld);
		Counts cTemp = null;
		if (errors.size() > BATCH_SIZE) {
			cTemp = save();
		}
		return cTemp;
	}

	private Integer saveErrors() {
		countFail = 0;
		int[] insMem = getJdbcTemplate().batchUpdate(INSERT_INTAKE_ERROR_DETAIL, new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				int i = 1;
				FileLoadDetail m = errors.get(idx);
				setParameter(i++, ps, m.getFileLoadSummaryId());
				setParameter(i++, ps, m.getRecordNumber());
				setParameter(i++, ps, m.getStatus());
				setParameter(i++, ps, m.getRemark());
				if (m.getStatus() == 1) {
					countFail++;
				}
			}

			@Override
			public int getBatchSize() {
				return errors.size();
			}
		});
		int recordsInserted = 0;
		for (int i : insMem) {
			recordsInserted += i;
		}
		logger.info("Errors Inserted: " + recordsInserted);

		return countFail;
	}

	public void batchProcessProviders(final List<ProviderLoad> providers) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insProv = jdbcTemplate.batchUpdate(INSERT_PROVIDER_QUERY, new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				int i = 1;
				ProviderLoad p = providers.get(idx);

				setParameter(i++, ps, p.getHcid());
				setParameter(i++, ps, p.getCompanyCount());
				setParameter(i++, ps, p.getDeltaDate());
				setParameter(i++, ps, p.getProviderId());
				setParameter(i++, ps, p.getRecordType());
				setParameter(i++, ps, p.getProviderType());
				setParameter(i++, ps, p.getLoaderCompositeKey());
				setParameter(i++, ps, p.getDataIntakeFileId());
				setParameter(i++, ps, p.getRecordNumber());
			}

			@Override
			public int getBatchSize() {
				return providers.size();
			}
		});
		int recordsInserted = 0;
		for (int i : insProv) {
			recordsInserted += i;
		}
		logger.info("Provider Records Inserted: " + recordsInserted);
	}

	public void batchProcessProviderAddresses(final List<ProviderAddressLoad> addresses) {
		int[] insProv = getJdbcTemplate().batchUpdate(INSERT_PROVIDER_ADDRESS_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						ProviderAddressLoad pa = addresses.get(idx);

						setParameter(i++, ps, pa.getAddressId());
						setParameter(i++, ps, pa.getHcid());
						setParameter(i++, ps, pa.getAddress1());
						setParameter(i++, ps, pa.getAddress2());
						setParameter(i++, ps, pa.getCity());
						setParameter(i++, ps, pa.getCompanyCount());
						setParameter(i++, ps, pa.getCounty());
						setParameter(i++, ps, pa.getDeltaDate());
						setParameter(i++, ps, pa.getDpv());
						setParameter(i++, ps, pa.getGeoReturn());
						setParameter(i++, ps, pa.getLatitude());
						setParameter(i++, ps, pa.getLongitude());
						setParameter(i++, ps, pa.getProviderId());
						setParameter(i++, ps, pa.getRank());
						setParameter(i++, ps, pa.getRecordType());
						setParameter(i++, ps, pa.getState());
						setParameter(i++, ps, pa.getTierCode());
						setParameter(i++, ps, pa.getVerificationCode());
						setParameter(i++, ps, pa.getVerificationDate());
						setParameter(i++, ps, pa.getZip());
						setParameter(i++, ps, pa.getZip4());
						setParameter(i++, ps, pa.getLoaderCompositeKey());
						setParameter(i++, ps, pa.getDataIntakeFileId());
						setParameter(i++, ps, pa.getRecordNumber());
					}

					@Override
					public int getBatchSize() {
						return providerAddresses.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insProv) {
			recordsInserted += i;
		}
		logger.info("Provider Address Records Inserted: " + recordsInserted);
	}

	public void batchProcessProviderAddressMedicaid(final List<ProviderAddressMedicaidLoad> addrMedicaid) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insProv = jdbcTemplate.batchUpdate(INSERT_PROVIDER_ADDRESS_MEDICAID_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						ProviderAddressMedicaidLoad p = addrMedicaid.get(idx);

						setParameter(i++, ps, p.getAddressId());
						setParameter(i++, ps, p.getHcid());
						setParameter(i++, ps, p.getMedicaidId());
						setParameter(i++, ps, p.getState());
						setParameter(i++, ps, p.getCompanyCount());
						setParameter(i++, ps, p.getDeltaDate());
						setParameter(i++, ps, p.getProviderId());
						setParameter(i++, ps, p.getRecordType());
						setParameter(i++, ps, p.getTierCode());
						setParameter(i++, ps, p.getVerificationCode());
						setParameter(i++, ps, p.getVerificationDate());
						setParameter(i++, ps, p.getLoaderCompositeKey());
						setParameter(i++, ps, p.getDataIntakeFileId());
						setParameter(i++, ps, p.getRecordNumber());
					}

					@Override
					public int getBatchSize() {
						System.out.println("---providerAddressesMedicaid---" + providerAddressesMedicaid.size());
						return providerAddressesMedicaid.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insProv) {
			recordsInserted += i;
		}
		logger.info("ProviderAddressMedicaid Records Inserted: " + recordsInserted);
	}

	public void batchProcessProviderAddressSPI(final List<ProviderAddressSPILoad> addrSpiPhone) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insProv = jdbcTemplate.batchUpdate(INSERT_PROVIDER_ADDRESS_SPI_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						ProviderAddressSPILoad p = addrSpiPhone.get(idx);

						setParameter(i++, ps, p.getAddressId());
						setParameter(i++, ps, p.getHcid());
						setParameter(i++, ps, p.getActiveEndDate());
						setParameter(i++, ps, p.getCancel());
						setParameter(i++, ps, p.getCcr());
						setParameter(i++, ps, p.getCensus());
						setParameter(i++, ps, p.getChange());
						setParameter(i++, ps, p.getCompanyCount());
						setParameter(i++, ps, p.getDeltaDate());
						setParameter(i++, ps, p.getEligibility());
						setParameter(i++, ps, p.getMedHistory());
						setParameter(i++, ps, p.getNew1());
						setParameter(i++, ps, p.getProviderId());
						setParameter(i++, ps, p.getReSupp());
						setParameter(i++, ps, p.getRecordType());
						setParameter(i++, ps, p.getRefill());
						setParameter(i++, ps, p.getRxFill());
						setParameter(i++, ps, p.getServiceLevel10());
						setParameter(i++, ps, p.getServiceLevel11());
						setParameter(i++, ps, p.getServiceLevel12());
						setParameter(i++, ps, p.getServiceLevel13());
						setParameter(i++, ps, p.getServiceLevel14());
						setParameter(i++, ps, p.getServiceLevel15());
						setParameter(i++, ps, p.getServiceLevelCode());
						setParameter(i++, ps, p.getSpiLocationCode());
						setParameter(i++, ps, p.getSpiRoot());
						setParameter(i++, ps, p.getTierCode());
						setParameter(i++, ps, p.getVerificationCode());
						setParameter(i++, ps, p.getVerificationDate());
						setParameter(i++, ps, p.getLoaderCompositeKey());
						setParameter(i++, ps, p.getDataIntakeFileId());
						setParameter(i++, ps, p.getRecordNumber());
					}

					@Override
					public int getBatchSize() {
						return providerAddressesSPI.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insProv) {
			recordsInserted += i;
		}
		logger.info("providerAddressesSPI Records Inserted: " + recordsInserted);
	}

	public void batchProcessProviderAddressPhone(final List<ProviderAddressPhoneLoad> addrPhone) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insProv = jdbcTemplate.batchUpdate(INSERT_PROVIDER_ADDRESS_PHONE_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						ProviderAddressPhoneLoad p = addrPhone.get(idx);

						setParameter(i++, ps, p.getHcid());
						setParameter(i++, ps, p.getProviderId());
						setParameter(i++, ps, p.getAddressId());
						setParameter(i++, ps, p.getPhoneType());
						setParameter(i++, ps, p.getPhoneNumber());
						setParameter(i++, ps, p.getCompanyCount());
						setParameter(i++, ps, p.getTierCode());
						setParameter(i++, ps, p.getVerificationCode());
						setParameter(i++, ps, p.getVerificationDate());
						setParameter(i++, ps, p.getAddrPhoneRank());
						setParameter(i++, ps, p.getRecordType());
						setParameter(i++, ps, p.getDeltaDate());
						setParameter(i++, ps, p.getLoaderCompositeKey());
						setParameter(i++, ps, p.getDataIntakeFileId());
						setParameter(i++, ps, p.getRecordNumber());
					}

					@Override
					public int getBatchSize() {
						System.out.println(
								"----providerAddressesPhone size is -------- " + providerAddressesPhone.size());
						return providerAddressesPhone.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insProv) {
			recordsInserted += i;
		}
		logger.info("ProviderAddressPhone Records Inserted: " + recordsInserted);
	}
	
	public void batchProcessProviderBirthDate(final List<ProviderBirthDateLoad> provBirthDate) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insProv = jdbcTemplate.batchUpdate(INSERT_PROVIDER_BIRTH_DATE_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						ProviderBirthDateLoad p = provBirthDate.get(idx);

						setParameter(i++, ps, p.getHcid());
						setParameter(i++, ps, p.getBirthDate());
						setParameter(i++, ps, p.getCompanyCount());
						setParameter(i++, ps, p.getDeltaDate());
						setParameter(i++, ps, p.getProviderId());
						setParameter(i++, ps, p.getRecordType());
						setParameter(i++, ps, p.getTierCode());
						setParameter(i++, ps, p.getVerificationCode());
						setParameter(i++, ps, p.getVerificationDate());
						setParameter(i++, ps, p.getLoaderCompositeKey());
						setParameter(i++, ps, p.getDataIntakeFileId());
						setParameter(i++, ps, p.getRecordNumber());
					}

					@Override
					public int getBatchSize() {
						return providerBirthDate.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insProv) {
			recordsInserted += i;
		}
		logger.info("ProviderBirthDateLoad Records Inserted: " + recordsInserted);
	}
	
	public void batchProcessProviderDeceasedData(final List<ProviderDeceasedDataLoad> provDeceased) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insProv = jdbcTemplate.batchUpdate(INSERT_PROVIDER_DECEASED_DATA_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						ProviderDeceasedDataLoad p = provDeceased.get(idx);

						setParameter(i++, ps, p.getDeceasedIndicator());
						setParameter(i++, ps, p.getHcid());
						setParameter(i++, ps, p.getDeceasedDate());
						setParameter(i++, ps, p.getDeltaDate());
						setParameter(i++, ps, p.getProviderId());
						setParameter(i++, ps, p.getRecordType());
						setParameter(i++, ps, p.getLoaderCompositeKey());
						setParameter(i++, ps, p.getDataIntakeFileId());
						setParameter(i++, ps, p.getRecordNumber());
					}

					@Override
					public int getBatchSize() {
						return providerDeceasedData.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insProv) {
			recordsInserted += i;
		}
		logger.info("ProviderDeceasedDataLoad Records Inserted: " + recordsInserted);
	}
	
	public void batchProcessProviderDEANumber(final List<ProviderDEANumberLoad> provDeaNumber) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insProv = jdbcTemplate.batchUpdate(INSERT_PROVIDER_DEA_NUMBER_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						ProviderDEANumberLoad p = provDeaNumber.get(idx);

						setParameter(i++, ps, p.getDeaNumber());
						setParameter(i++, ps, p.getHcid());
						setParameter(i++, ps, p.getCompanyCount());
						setParameter(i++, ps, p.getDeaStatusCode());
						setParameter(i++, ps, p.getDeltaDate());
						setParameter(i++, ps, p.getDrugSchedule());
						setParameter(i++, ps, p.getProviderId());
						setParameter(i++, ps, p.getRecordType());
						setParameter(i++, ps, p.getRenewalDate());
						setParameter(i++, ps, p.getTierCode());
						setParameter(i++, ps, p.getVerificationCode());
						setParameter(i++, ps, p.getVerificationDate());
						setParameter(i++, ps, p.getLoaderCompositeKey());
						setParameter(i++, ps, p.getDataIntakeFileId());
						setParameter(i++, ps, p.getRecordNumber());
					}

					@Override
					public int getBatchSize() {
						return providerDEANumber.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insProv) {
			recordsInserted += i;
		}
		logger.info("ProviderDEANumberLoad Records Inserted: " + recordsInserted);
	}
	
	public void batchProcessProviderDEANumberDataWaiver(final List<ProviderDEANumberDataWaiverLoad> provDeaNumberDataWaiver) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insProv = jdbcTemplate.batchUpdate(INSERT_PROVIDER_DEA_NUMBER_DATA_WAIVER_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						ProviderDEANumberDataWaiverLoad p = provDeaNumberDataWaiver.get(idx);

						setParameter(i++, ps, p.getDeaNumber());
						setParameter(i++, ps, p.getHcid());
						setParameter(i++, ps, p.getActivityCode());
						setParameter(i++, ps, p.getDeltaDate());
						setParameter(i++, ps, p.getProviderId());
						setParameter(i++, ps, p.getRecordType());
						setParameter(i++, ps, p.getVerificationDate());
						setParameter(i++, ps, p.getLoaderCompositeKey());
						setParameter(i++, ps, p.getDataIntakeFileId());
						setParameter(i++, ps, p.getRecordNumber());
					}

					@Override
					public int getBatchSize() {
						return providerDEANumberDataWaiver.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insProv) {
			recordsInserted += i;
		}
		logger.info("ProviderDEANumberDataWaiverLoad Records Inserted: " + recordsInserted);
	}
	
	public void batchProcessProviderDegree(final List<ProviderDegreeLoad> provDegree) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insProv = jdbcTemplate.batchUpdate(INSERT_PROVIDER_DEGREE_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						ProviderDegreeLoad p = provDegree.get(idx);

						setParameter(i++, ps, p.getDegree());
						setParameter(i++, ps, p.getHcid());
						setParameter(i++, ps, p.getCompanyCount());
						setParameter(i++, ps, p.getDeltaDate());
						setParameter(i++, ps, p.getProviderId());
						setParameter(i++, ps, p.getRecordType());
						setParameter(i++, ps, p.getTierCode());
						setParameter(i++, ps, p.getVerificationDate());
						setParameter(i++, ps, p.getVerificationCode());
						setParameter(i++, ps, p.getLoaderCompositeKey());
						setParameter(i++, ps, p.getDataIntakeFileId());
						setParameter(i++, ps, p.getRecordNumber());
					}

					@Override
					public int getBatchSize() {
						return providerDegree.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insProv) {
			recordsInserted += i;
		}
		logger.info("ProviderDegreeLoad Records Inserted: " + recordsInserted);
	}
	
	public void batchProcessProviderGender(final List<ProviderGenderLoad> provGender) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insProv = jdbcTemplate.batchUpdate(INSERT_PROVIDER_GENDER_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						ProviderGenderLoad p = provGender.get(idx);

						setParameter(i++, ps, p.getHcid());
						setParameter(i++, ps, p.getCompanyCount());
						setParameter(i++, ps, p.getDeltaDate());
						setParameter(i++, ps, p.getGender());
						setParameter(i++, ps, p.getProviderId());
						setParameter(i++, ps, p.getRecordType());
						setParameter(i++, ps, p.getTierCode());
						setParameter(i++, ps, p.getVerificationCode());
						setParameter(i++, ps, p.getVerificationDate());
						setParameter(i++, ps, p.getLoaderCompositeKey());
						setParameter(i++, ps, p.getDataIntakeFileId());
						setParameter(i++, ps, p.getRecordNumber());
					}

					@Override
					public int getBatchSize() {
						return providerGender.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insProv) {
			recordsInserted += i;
		}
		logger.info("ProviderGenderLoad Records Inserted: " + recordsInserted);
	}
	
	public void batchProcessProviderHCID(final List<ProviderHCIDLoad> provHCID) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insProv = jdbcTemplate.batchUpdate(INSERT_PROVIDER_HCID_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						ProviderHCIDLoad p = provHCID.get(idx);

						setParameter(i++, ps, p.getHcid());
						setParameter(i++, ps, p.getDeltaDate());
						setParameter(i++, ps, p.getProviderId());
						setParameter(i++, ps, p.getRecordType());
						setParameter(i++, ps, p.getLoaderCompositeKey());
						setParameter(i++, ps, p.getDataIntakeFileId());
						setParameter(i++, ps, p.getRecordNumber());
					}

					@Override
					public int getBatchSize() {
						return providerHCID.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insProv) {
			recordsInserted += i;
		}
		logger.info("ProviderHCIDLoad Records Inserted: " + recordsInserted);
	}
	
	
	public void batchProcessProviderName(final List<ProviderNameLoad> provName) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insProv = jdbcTemplate.batchUpdate(INSERT_PROVIDER_NAME_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						ProviderNameLoad p = provName.get(idx);

						setParameter(i++, ps, p.getHcid());
						setParameter(i++, ps, p.getCompanyCount());
						setParameter(i++, ps, p.getDeltaDate());
						setParameter(i++, ps, p.getFirstName());
						setParameter(i++, ps, p.getMiddleName());
						setParameter(i++, ps, p.getLastName());
						setParameter(i++, ps, p.getProviderId());
						setParameter(i++, ps, p.getRecordType());
						setParameter(i++, ps, p.getSuffix());
						setParameter(i++, ps, p.getTierCode());
						setParameter(i++, ps, p.getVerificationCode());
						setParameter(i++, ps, p.getVerificationDate());
						setParameter(i++, ps, p.getLoaderCompositeKey());
						setParameter(i++, ps, p.getDataIntakeFileId());
						setParameter(i++, ps, p.getRecordNumber());
					}

					@Override
					public int getBatchSize() {
						return providerName.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insProv) {
			recordsInserted += i;
		}
		logger.info("ProviderNameLoad Records Inserted: " + recordsInserted);
	}
	
	
	public void batchProcessProviderNPI(final List<ProviderNPILoad> provNPI) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insProv = jdbcTemplate.batchUpdate(INSERT_PROVIDER_NPI_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						ProviderNPILoad p = provNPI.get(idx);

						setParameter(i++, ps, p.getHcid());
						setParameter(i++, ps, p.getCompanyCount());
						setParameter(i++, ps, p.getDeactivationDate());
						setParameter(i++, ps, p.getDeltaDate());
						setParameter(i++, ps, p.getNpi());
						setParameter(i++, ps, p.getProviderId());
						setParameter(i++, ps, p.getNpiRank());
						setParameter(i++, ps, p.getReactivationDate());
						setParameter(i++, ps, p.getRecordType());
						setParameter(i++, ps, p.getTierCode());
						setParameter(i++, ps, p.getVerificationCode());
						setParameter(i++, ps, p.getVerificationDate());
						setParameter(i++, ps, p.getLoaderCompositeKey());
						setParameter(i++, ps, p.getDataIntakeFileId());
						setParameter(i++, ps, p.getRecordNumber());
					}

					@Override
					public int getBatchSize() {
						return providerNPI.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insProv) {
			recordsInserted += i;
		}
		logger.info("ProviderNPILoad Records Inserted: " + recordsInserted);
	}
	
	
	public void batchProcessProviderSpecialty(final List<ProviderSpecialtyLoad> provSpecialty) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insProv = jdbcTemplate.batchUpdate(INSERT_PROVIDER_SPECIALTY_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						ProviderSpecialtyLoad p = provSpecialty.get(idx);

						setParameter(i++, ps, p.getHcid());
						setParameter(i++, ps, p.getSpecialtyDescription());
						setParameter(i++, ps, p.getCompanyCount());
						setParameter(i++, ps, p.getDeltaDate());
						setParameter(i++, ps, p.getProviderId());
						setParameter(i++, ps, p.getSpecialtyRank());
						setParameter(i++, ps, p.getRecordType());
						setParameter(i++, ps, p.getTierCode());
						setParameter(i++, ps, p.getVerificationCode());
						setParameter(i++, ps, p.getVerificationDate());
						setParameter(i++, ps, p.getLoaderCompositeKey());
						setParameter(i++, ps, p.getDataIntakeFileId());
						setParameter(i++, ps, p.getRecordNumber());
					}

					@Override
					public int getBatchSize() {
						return providerSpecialty.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insProv) {
			recordsInserted += i;
		}
		logger.info("ProviderSpecialtyLoad Records Inserted: " + recordsInserted);
	}
	
	public void batchProcessProviderStateLicense(final List<ProviderStateLicenseLoad> provStateLicense) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insProv = jdbcTemplate.batchUpdate(INSERT_PROVIDER_STATE_LICENSE_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						ProviderStateLicenseLoad p = provStateLicense.get(idx);

						setParameter(i++, ps, p.getHcid());
						setParameter(i++, ps, p.getLicenseNumber());
						setParameter(i++, ps, p.getLicenseState());
						setParameter(i++, ps, p.getCompanyCount());
						setParameter(i++, ps, p.getDeltaDate());
						setParameter(i++, ps, p.getLicenseExpirationDate());
						setParameter(i++, ps, p.getLicenseIssueDate());
						setParameter(i++, ps, p.getProviderId());
						setParameter(i++, ps, p.getRecordType());
						setParameter(i++, ps, p.getTierCode());
						setParameter(i++, ps, p.getVerificationCode());
						setParameter(i++, ps, p.getVerificationDate());
						setParameter(i++, ps, p.getLoaderCompositeKey());
						setParameter(i++, ps, p.getDataIntakeFileId());
						setParameter(i++, ps, p.getRecordNumber());
					}

					@Override
					public int getBatchSize() {
						return providerStateLicense.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insProv) {
			recordsInserted += i;
		}
		logger.info("ProviderStateLicenseLoad Records Inserted: " + recordsInserted);
	}
	
	public void batchProcessProviderTaxonomy(final List<ProviderTaxonomyLoad> provTaxonomy) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insProv = jdbcTemplate.batchUpdate(INSERT_PROVIDER_TAXONOMY_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						ProviderTaxonomyLoad p = provTaxonomy.get(idx);

						setParameter(i++, ps, p.getHcid());
						setParameter(i++, ps, p.getTaxonomyCode());
						setParameter(i++, ps, p.getCompanyCount());
						setParameter(i++, ps, p.getDeltaDate());
						setParameter(i++, ps, p.getPrimaryTaxonomyIndicator());
						setParameter(i++, ps, p.getProviderId());
						setParameter(i++, ps, p.getRecordType());
						setParameter(i++, ps, p.getTaxonomyDescription());
						setParameter(i++, ps, p.getTierCode());
						setParameter(i++, ps, p.getVerificationCode());
						setParameter(i++, ps, p.getVerificationDate());
						setParameter(i++, ps, p.getLoaderCompositeKey());
						setParameter(i++, ps, p.getDataIntakeFileId());
						setParameter(i++, ps, p.getRecordNumber());
					}

					@Override
					public int getBatchSize() {
						return providerTaxonomy.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insProv) {
			recordsInserted += i;
		}
		logger.info("ProviderTaxonomyLoad Records Inserted: " + recordsInserted);
	}
	
	
	public void batchProcessProviderUPIN(final List<ProviderUPINLoad> provUPIN) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insProv = jdbcTemplate.batchUpdate(INSERT_PROVIDER_UPIN_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						ProviderUPINLoad p = provUPIN.get(idx);

						setParameter(i++, ps, p.getHcid());
						setParameter(i++, ps, p.getUpin());
						setParameter(i++, ps, p.getCompanyCount());
						setParameter(i++, ps, p.getDeltaDate());
						setParameter(i++, ps, p.getProviderId());
						setParameter(i++, ps, p.getRecordType());
						setParameter(i++, ps, p.getTierCode());
						setParameter(i++, ps, p.getVerificationCode());
						setParameter(i++, ps, p.getVerificationDate());
						setParameter(i++, ps, p.getLoaderCompositeKey());
						setParameter(i++, ps, p.getDataIntakeFileId());
						setParameter(i++, ps, p.getRecordNumber());
					}

					@Override
					public int getBatchSize() {
						return providerUPIN.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insProv) {
			recordsInserted += i;
		}
		logger.info("ProviderUPINLoad Records Inserted: " + recordsInserted);
	}
	
	public void batchProcessChangeOfOwner(final List<ChangeOfOwnershipInformationLoad> changeOfOwner) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insPharm = jdbcTemplate.batchUpdate(INSERT_CHANGE_OF_OWNER_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						ChangeOfOwnershipInformationLoad coo = changeOfOwner.get(idx);

						setParameter(i++, ps,coo.getNcpdpproviderid());
						setParameter(i++, ps,coo.getChangeOfOwnershipEffectiveDate());
						setParameter(i++, ps,coo.getFiller());
						setParameter(i++, ps,coo.getOldNcpdpProviderId());
						setParameter(i++, ps,coo.getOldStoreCloseDate());
						setParameter(i++, ps,coo.getDataIntakeFileId());
						setParameter(i++, ps,coo.getRecordNumber());
						setParameter(i++, ps,coo.getLoaderCompositeKey());
					}

					@Override
					public int getBatchSize() {
						return changeOfOwner.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insPharm) {
			recordsInserted += i;
		}
		logger.info("ChangeOfOwnershipInformationLoad Records Inserted: " + recordsInserted);
	}
	
	
	public void batchProcessEprescribing(final List<EprescribingInformationLoad> epres) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insPharm = jdbcTemplate.batchUpdate(INSERT_EPRESCRIBING_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						EprescribingInformationLoad epr = epres.get(idx);

						setParameter(i++, ps,epr.getNcpdpproviderid());
						setParameter(i++, ps,epr.getNetworkIdentifier());
						setParameter(i++, ps,epr.getServiceLevelCodes());
						setParameter(i++, ps,epr.getEffectiveFromDate());
						setParameter(i++, ps,epr.getEffectiveThroughDate());
						setParameter(i++, ps,epr.getFiller());
						setParameter(i++, ps,epr.getDataIntakeFileId());
						setParameter(i++, ps,epr.getRecordNumber());
						setParameter(i++, ps,epr.getLoaderCompositeKey());
					}

					@Override
					public int getBatchSize() {
						return eprescribing.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insPharm) {
			recordsInserted += i;
		}
		logger.info("EprescribingInformationLoad Records Inserted: " + recordsInserted);
	}

	
	public void batchProcessFraudWasteAndATA(final List<FraudWasteAndAbuseTrainingAttestationLoad> fraudWaste) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insPharm = jdbcTemplate.batchUpdate(INSERT_FRAUD_WASTE_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						FraudWasteAndAbuseTrainingAttestationLoad fwa = fraudWaste.get(idx);

						setParameter(i++, ps,fwa.getNcpdpproviderid());
						setParameter(i++, ps,fwa.getAccreditationDate());
						setParameter(i++, ps,fwa.getAccreditationOrganization());
						setParameter(i++, ps,fwa.getAddress1());
						setParameter(i++, ps,fwa.getAddress2());
						setParameter(i++, ps,fwa.getCity());
						setParameter(i++, ps,fwa.getEmail());
						setParameter(i++, ps,fwa.getFax());
						setParameter(i++, ps,fwa.getFiller());
						setParameter(i++, ps,fwa.getFwaAttestation());
						setParameter(i++, ps,fwa.getMedicarePartd());
						setParameter(i++, ps,fwa.getNpi());
						setParameter(i++, ps,fwa.getPsaoName());
						setParameter(i++, ps,fwa.getPlanYear());
						setParameter(i++, ps,fwa.getQ1());
						setParameter(i++, ps,fwa.getQ2());
						setParameter(i++, ps,fwa.getQ3());
						setParameter(i++, ps,fwa.getQ4());
						setParameter(i++, ps,fwa.getResponsibleParty());
						setParameter(i++, ps,fwa.getSignatureDate());
						setParameter(i++, ps,fwa.getSignatureOfResponsibleParty());
						setParameter(i++, ps,fwa.getState());
						setParameter(i++, ps,fwa.getVersionNumber());
						setParameter(i++, ps,fwa.getZipCode());
						setParameter(i++, ps,fwa.getDataIntakeFileId());
						setParameter(i++, ps,fwa.getRecordNumber());
						setParameter(i++, ps,fwa.getLoaderCompositeKey());
					}

					@Override
					public int getBatchSize() {
						return fraudWasteAndATA.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insPharm) {
			recordsInserted += i;
		}
		logger.info("FraudWasteAndAbuseTrainingAttestationLoad Records Inserted: " + recordsInserted);
	}

	
	public void batchProcessParentOrg(final List<ParentOrganizationInformationLoad> parentOrgnstn) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insPharm = jdbcTemplate.batchUpdate(INSERT_PARENT_ORG_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						ParentOrganizationInformationLoad paOrg = parentOrgnstn.get(idx);

						setParameter(i++, ps,paOrg.getParentOrganizationId());
						setParameter(i++, ps,paOrg.getAddress1());
						setParameter(i++, ps,paOrg.getAddress2());
						setParameter(i++, ps,paOrg.getCity());
						setParameter(i++, ps,paOrg.getContactName());
						setParameter(i++, ps,paOrg.getContactTitle());
						setParameter(i++, ps,paOrg.getDeleteDate());
						setParameter(i++, ps,paOrg.getEmailAddress());
						setParameter(i++, ps,paOrg.getExtension());
						setParameter(i++, ps,paOrg.getFaxNumber());
						setParameter(i++, ps,paOrg.getFiller());
						setParameter(i++, ps,paOrg.getParentOrganizationFederalTaxId());
						setParameter(i++, ps,paOrg.getParentOrganizationName());
						setParameter(i++, ps,paOrg.getParentOrganizationNpi());
						setParameter(i++, ps,paOrg.getPhoneNumber());
						setParameter(i++, ps,paOrg.getStateCode());
						setParameter(i++, ps,paOrg.getZipCode());
						setParameter(i++, ps,paOrg.getDataIntakeFileId());
						setParameter(i++, ps,paOrg.getRecordNumber());
						setParameter(i++, ps,paOrg.getLoaderCompositeKey());
					}

					@Override
					public int getBatchSize() {
						return parentOrg.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insPharm) {
			recordsInserted += i;
		}
		logger.info("ParentOrganizationInformationLoad Records Inserted: " + recordsInserted);
	}

	
	public void batchProcessPaymentCentre(final List<PaymentCenterInformationLoad> pharmPaymentCentre) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insPharm = jdbcTemplate.batchUpdate(INSERT_PAYMENT_CENTRE_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						PaymentCenterInformationLoad pc = pharmPaymentCentre.get(idx);

						setParameter(i++, ps,pc.getPaymentCenterId());
						setParameter(i++, ps,pc.getDeleteDate());
						setParameter(i++, ps,pc.getFiller());
						setParameter(i++, ps,pc.getPaymentCenterAddress1());
						setParameter(i++, ps,pc.getPaymentCenterAddress2());
						setParameter(i++, ps,pc.getPaymentCenterCity());
						setParameter(i++, ps,pc.getPaymentCenterContactName());
						setParameter(i++, ps,pc.getPaymentCenterContactTitle());
						setParameter(i++, ps,pc.getPaymentCenteremailAddress());
						setParameter(i++, ps,pc.getPaymentCenterExtension());
						setParameter(i++, ps,pc.getPaymentCenterFaxNumber());
						setParameter(i++, ps,pc.getPaymentCenterFederalTaxId());
						setParameter(i++, ps,pc.getPaymentCenterName());
						setParameter(i++, ps,pc.getPaymentCenterNpi());
						setParameter(i++, ps,pc.getPaymentCenterPhoneNumber());
						setParameter(i++, ps,pc.getPaymentCenterStateCode());
						setParameter(i++, ps,pc.getPaymentCenterZipCode());
						setParameter(i++, ps,pc.getDataIntakeFileId());
						setParameter(i++, ps,pc.getRecordNumber());
						setParameter(i++, ps,pc.getLoaderCompositeKey());
					}

					@Override
					public int getBatchSize() {
						return paymentCentre.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insPharm) {
			recordsInserted += i;
		}
		logger.info("PaymentCenterInformationLoad Records Inserted: " + recordsInserted);
	}

	
	public void batchProcessProviderInfo(final List<ProviderInformationLoad> provInfo) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insPharm = jdbcTemplate.batchUpdate(INSERT_PROVIDER_INFO_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						ProviderInformationLoad pi = provInfo.get(idx);

						setParameter(i++, ps,pi.getNcpdpproviderid());
						setParameter(i++, ps,pi.getContactEmailAddress());
						setParameter(i++, ps,pi.getContactExtension());
						setParameter(i++, ps,pi.getContactFirstName());
						setParameter(i++, ps,pi.getContactLastName());
						setParameter(i++, ps,pi.getContactMiddleInitial());
						setParameter(i++, ps,pi.getContactPhoneNumber());
						setParameter(i++, ps,pi.getContactTitle());
						setParameter(i++, ps,pi.getDeaExpirationDate());
						setParameter(i++, ps,pi.getDeaRegistrationId());
						setParameter(i++, ps,pi.getDeactivationCode());
						setParameter(i++, ps,pi.getDispenserClassCode());
						setParameter(i++, ps,pi.getDoctorName());
						setParameter(i++, ps,pi.getFederalTaxidnumber());
						setParameter(i++, ps,pi.getFiller());
						setParameter(i++, ps,pi.getLegalBusinessName());
						setParameter(i++, ps,pi.getMailingAddress1());
						setParameter(i++, ps,pi.getMailingAddress2());
						setParameter(i++, ps,pi.getMailingAddressCity());
						setParameter(i++, ps,pi.getMailingAddressStateCode());
						setParameter(i++, ps,pi.getMailingAddressZipCode());
						setParameter(i++, ps,pi.getMedicareProviderid());
						setParameter(i++, ps,pi.getName());
						setParameter(i++, ps,pi.getNpi());
						setParameter(i++, ps,pi.getPhysicalLocation24hourOperationFlag());
						setParameter(i++, ps,pi.getPhysicalLocationAddress1());
						setParameter(i++, ps,pi.getPhysicalLocationAddress2());
						setParameter(i++, ps,pi.getPhysicalLocationCity());
						setParameter(i++, ps,pi.getPhysicalLocationCongressionalVotingDistrict());
						setParameter(i++, ps,pi.getPhysicalLocationCountryParish());
						setParameter(i++, ps,pi.getPhysicalLocationCrossStreet());
						setParameter(i++, ps,pi.getPhysicalLocationEmailAddress());
						setParameter(i++, ps,pi.getPhysicalLocationExtension());
						setParameter(i++, ps,pi.getPhysicalLocationFax());
						setParameter(i++, ps,pi.getPhysicalLocationLanguageCode1());
						setParameter(i++, ps,pi.getPhysicalLocationLanguageCode2());
						setParameter(i++, ps,pi.getPhysicalLocationLanguageCode3());
						setParameter(i++, ps,pi.getPhysicalLocationLanguageCode4());
						setParameter(i++, ps,pi.getPhysicalLocationLanguageCode5());
						setParameter(i++, ps,pi.getPhysicalLocationMsa());
						setParameter(i++, ps,pi.getPhysicalLocationPmsa());
						setParameter(i++, ps,pi.getPhysicalLocationPhoneNumber());
						setParameter(i++, ps,pi.getPhysicalLocationProviderHours());
						setParameter(i++, ps,pi.getPhysicalLocationStateCode());
						setParameter(i++, ps,pi.getPhysicalLocationStoreClosureDate());
						setParameter(i++, ps,pi.getPhysicalLocationStoreOpenDate());
						setParameter(i++, ps,pi.getPhysicalLocationZipCode());
						setParameter(i++, ps,pi.getPrimaryProviderTypeCode());
						setParameter(i++, ps,pi.getReinstatementCode());
						setParameter(i++, ps,pi.getReinstatementDate());
						setParameter(i++, ps,pi.getSecondaryProviderTypeCode());
						setParameter(i++, ps,pi.getStateIncomeTaxidnumber());
						setParameter(i++, ps,pi.getStoreNumber());
						setParameter(i++, ps,pi.getTertiaryProviderTypeCode());
						setParameter(i++, ps,pi.getTransactionCode());
						setParameter(i++, ps,pi.getTransactionDate());
						setParameter(i++, ps,pi.getDataIntakeFileId());
						setParameter(i++, ps,pi.getRecordNumber());
						setParameter(i++, ps,pi.getLoaderCompositeKey());
						System.out.println("to string"  + pi.toString()) ;
					}

					@Override
					public int getBatchSize() {
						return provInfo.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insPharm) {
			recordsInserted += i;
		}
		logger.info("ProviderInformationLoad Records Inserted: " + recordsInserted);
	}

	
	public void batchProcessProviderRelInfo(final List<ProviderRelationshipInformationLoad> provRel) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insPharm = jdbcTemplate.batchUpdate(INSERT_PROV_REL_INFO_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						ProviderRelationshipInformationLoad prel = provRel.get(idx);

						setParameter(i++, ps,prel.getNcpdpproviderid());
						setParameter(i++, ps,prel.getEffectiveFromDate());
						setParameter(i++, ps,prel.getEffectiveThroughDate());
						setParameter(i++, ps,prel.getFiller());
						setParameter(i++, ps,prel.getIsPrimary());
						setParameter(i++, ps,prel.getPaymentCenterId());
						setParameter(i++, ps,prel.getProviderType());
						setParameter(i++, ps,prel.getRelationshipId());
						setParameter(i++, ps,prel.getRemitAndReconciliationId());
						setParameter(i++, ps,prel.getDataIntakeFileId());
						setParameter(i++, ps,prel.getRecordNumber());
						setParameter(i++, ps,prel.getLoaderCompositeKey());
					}

					@Override
					public int getBatchSize() {
						return provRel.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insPharm) {
			recordsInserted += i;
		}
		logger.info("ProviderRelationshipInformationLoad Records Inserted: " + recordsInserted);
	}

	
	public void batchProcessRelDemographic(final List<RelationshipDemographicInformationLoad> relDemo) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insPharm = jdbcTemplate.batchUpdate(INSERT_REL_DEMO_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						RelationshipDemographicInformationLoad rel = relDemo.get(idx);

						setParameter(i++, ps,rel.getRelationshipid());
						setParameter(i++, ps,rel.getAddress1());
						setParameter(i++, ps,rel.getAddress2());
						setParameter(i++, ps,rel.getAuditContactEmail());
						setParameter(i++, ps,rel.getAuditContactName());
						setParameter(i++, ps,rel.getAuditContactTitle());
						setParameter(i++, ps,rel.getCity());
						setParameter(i++, ps,rel.getContactName());
						setParameter(i++, ps,rel.getContactTitle());
						setParameter(i++, ps,rel.getContractualContactEmail());
						setParameter(i++, ps,rel.getContractualContactName());
						setParameter(i++, ps,rel.getAuditContactTitle());
						setParameter(i++, ps,rel.getDeleteDate());
						setParameter(i++, ps,rel.getEffectiveFromDate());
						setParameter(i++, ps,rel.getEmailAddress());
						setParameter(i++, ps,rel.getExtension());
						setParameter(i++, ps,rel.getFaxNumber());
						setParameter(i++, ps,rel.getFiller());
						setParameter(i++, ps,rel.getName());
						setParameter(i++, ps,rel.getOperationalContactEmail());
						setParameter(i++, ps,rel.getOperationalContactName());
						setParameter(i++, ps,rel.getOperationalContactTitle());
						setParameter(i++, ps,rel.getParentOrganizationId());
						setParameter(i++, ps,rel.getPhoneNumber());
						setParameter(i++, ps,rel.getRelationshipFederalTaxId());
						setParameter(i++, ps,rel.getRelationshipNpi());
						setParameter(i++, ps,rel.getRelationshipType());
						setParameter(i++, ps,rel.getStateCode());
						setParameter(i++, ps,rel.getTechnicalContactEmail());
						setParameter(i++, ps,rel.getTechnicalContactName());
						setParameter(i++, ps,rel.getTechnicalContactTitle());
						setParameter(i++, ps,rel.getZipCode());
						setParameter(i++, ps,rel.getDataIntakeFileId());
						setParameter(i++, ps,rel.getRecordNumber());
						setParameter(i++, ps,rel.getLoaderCompositeKey());
					}

					@Override
					public int getBatchSize() {
						return relDemo.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insPharm) {
			recordsInserted += i;
		}
		logger.info("RelationshipDemographicInformationLoad Records Inserted: " + recordsInserted);
	}

	
	public void batchProcessRemitAndReconciliation(final List<RemitAndReconciliationInformationLoad> remitRecon) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insPharm = jdbcTemplate.batchUpdate(INSERT_REMIT_RECON_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						RemitAndReconciliationInformationLoad rem = remitRecon.get(idx);

						setParameter(i++, ps,rem.getRemitAndReconciliationId());
						setParameter(i++, ps,rem.getDeleteDate());
						setParameter(i++, ps,rem.getFiller());
						setParameter(i++, ps,rem.getRemitAndReconciliationAddress1());
						setParameter(i++, ps,rem.getRemitAndReconciliationAddress2());
						setParameter(i++, ps,rem.getRemitAndReconciliationCity());
						setParameter(i++, ps,rem.getRemitAndReconciliationContactName());
						setParameter(i++, ps,rem.getRemitAndReconciliationContactTitle());
						setParameter(i++, ps,rem.getRemitAndReconciliationemailAddress());
						setParameter(i++, ps,rem.getRemitAndReconciliationExtension());
						setParameter(i++, ps,rem.getRemitAndReconciliationFaxNumber());
						setParameter(i++, ps,rem.getRemitAndReconciliationFederalTax());
						setParameter(i++, ps,rem.getRemitAndReconciliationName());
						setParameter(i++, ps,rem.getRemitAndReconciliationNpi());
						setParameter(i++, ps,rem.getRemitAndReconciliationPhoneNumber());
						setParameter(i++, ps,rem.getRemitAndReconciliationStateCode());
						setParameter(i++, ps,rem.getRemitAndReconciliationZipCode());
						setParameter(i++, ps,rem.getDataIntakeFileId());
						setParameter(i++, ps,rem.getRecordNumber());
						setParameter(i++, ps,rem.getLoaderCompositeKey());
					}

					@Override
					public int getBatchSize() {
						return remitAndReconciliation.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insPharm) {
			recordsInserted += i;
		}
		logger.info("RemitAndReconciliationInformationLoad Records Inserted: " + recordsInserted);
	}

	
	public void batchProcessServiceInfo(final List<ServicesInformationLoad> servInfo) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insPharm = jdbcTemplate.batchUpdate(INSERT_SERVICE_INFO_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						ServicesInformationLoad svc = servInfo.get(idx);

						setParameter(i++, ps,svc.getNcpdpproviderid());
						setParameter(i++, ps,svc.get_24hourEmergencyServiceIndicator());
						setParameter(i++, ps,svc.get_24hourEmergencyServiceCode());
						setParameter(i++, ps,svc.get_34bStatusIndicator());
						setParameter(i++, ps,svc.get_34bStatusCode());
						setParameter(i++, ps,svc.getAcceptseprescriptionsIndicator());
						setParameter(i++, ps,svc.getAcceptseprescriptionsCode());
						setParameter(i++, ps,svc.getClosedDoorFacilityIndicator());
						setParameter(i++, ps,svc.getClosedDoorFacilityStatusCode());
						setParameter(i++, ps,svc.getCompoundingServiceIndicator());
						setParameter(i++, ps,svc.getCompoundingServiceCode());
						setParameter(i++, ps,svc.getDeliveryServiceIndicator());
						setParameter(i++, ps,svc.getDeliveryServiceCode());
						setParameter(i++, ps,svc.getDriveUpWindowIndicator());
						setParameter(i++, ps,svc.getDriveUpWindowCode());
						setParameter(i++, ps,svc.getDurableMedicalEquipmentIndicator());
						setParameter(i++, ps,svc.getDurableMedicalEquipmentCode());
						setParameter(i++, ps,svc.getHandicappedAccessibleIndicator());
						setParameter(i++, ps,svc.getHandicappedAccessibleCode());
						setParameter(i++, ps,svc.getImmunizationsProvidedIndicator());
						setParameter(i++, ps,svc.getImmunizationsProvidedCode());
						setParameter(i++, ps,svc.getMultiDoseCompliancePackagingIndicator());
						setParameter(i++, ps,svc.getMultiDoseCompliancePackagingCode());
						setParameter(i++, ps,svc.getWalkInClinicIndicator());
						setParameter(i++, ps,svc.getWalkInClinicCode());
						setParameter(i++, ps,svc.getFiller());
						setParameter(i++, ps,svc.getDataIntakeFileId());
						setParameter(i++, ps,svc.getRecordNumber());
						setParameter(i++, ps,svc.getLoaderCompositeKey());
					}

					@Override
					public int getBatchSize() {
						return serviceInfo.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insPharm) {
			recordsInserted += i;
		}
		logger.info("ServicesInformationLoad Records Inserted: " + recordsInserted);
	}

	
	public void batchProcessStateMedicaid(final List<StateMedicaidInformationLoad> stateMed) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insPharm = jdbcTemplate.batchUpdate(INSERT_STATE_MEDICAID_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						StateMedicaidInformationLoad coo = stateMed.get(idx);

						setParameter(i++, ps,coo.getNcpdpproviderid());
						setParameter(i++, ps,coo.getDeleteDate());
						setParameter(i++, ps,coo.getFiller());
						setParameter(i++, ps,coo.getMedicaidId());
						setParameter(i++, ps,coo.getStateCode());
						setParameter(i++, ps,coo.getDataIntakeFileId());
						setParameter(i++, ps,coo.getRecordNumber());
						setParameter(i++, ps,coo.getLoaderCompositeKey());
					}

					@Override
					public int getBatchSize() {
						return stateMedicaid.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insPharm) {
			recordsInserted += i;
		}
		logger.info("StateMedicaidInformationLoad Records Inserted: " + recordsInserted);
	}

	
	public void batchProcessTaxonomyCode(final List<TaxonomyCodeInformationLoad> taxoCodeInfo) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insPharm = jdbcTemplate.batchUpdate(INSERT_TAXO_CODE_INFO_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						TaxonomyCodeInformationLoad coo = taxoCodeInfo.get(idx);

						setParameter(i++, ps,coo.getNcpdpproviderid());
						setParameter(i++, ps,coo.getDeleteDate());
						setParameter(i++, ps,coo.getFiller());
						setParameter(i++, ps,coo.getProviderTypeCode());
						setParameter(i++, ps,coo.getTaxonomyCode());
						setParameter(i++, ps,coo.getDataIntakeFileId());
						setParameter(i++, ps,coo.getRecordNumber());
						setParameter(i++, ps,coo.getLoaderCompositeKey());
					}

					@Override
					public int getBatchSize() {
						return taxonomyCode.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insPharm) {
			recordsInserted += i;
		}
		logger.info("TaxonomyCodeInformationLoad Records Inserted: " + recordsInserted);
	}


	public void batchProcessStateLicense(final List<StateLicenseInformationLoad> stateLicense) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		int[] insPharm = jdbcTemplate.batchUpdate(INSERT_STATE_LICENSE_QUERY,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int idx) throws SQLException {
						int i = 1;
						StateLicenseInformationLoad stl = stateLicense.get(idx);

						setParameter(i++, ps,stl.getNcpdpproviderid());
						setParameter(i++, ps,stl.getDeleteDate());
						setParameter(i++, ps,stl.getFiller());
						setParameter(i++, ps,stl.getLicenseStateCode());
						setParameter(i++, ps,stl.getStateLicenseExpirationDate());
						setParameter(i++, ps,stl.getStateLicenseNumber());
						setParameter(i++, ps,stl.getDataIntakeFileId());
						setParameter(i++, ps,stl.getRecordNumber());
						setParameter(i++, ps,stl.getLoaderCompositeKey());
					}

					@Override
					public int getBatchSize() {
						return stateLicense.size();
					}
				});
		int recordsInserted = 0;
		for (int i : insPharm) {
			recordsInserted += i;
		}
		logger.info("StateLicenseInformationLoad Records Inserted: " + recordsInserted);
	}

	private void setParameter(Integer i, PreparedStatement ps, Object val) {
		try {
			if (val != null) {
				if (val.getClass().toString().contains("String") || val.getClass().toString().contains("Char")) {
					String strVal = (String) val;
					ps.setString(i, strVal);
				} else if (val.getClass().toString().contains("Long")) {
					Long lngVal = (Long) val;
					ps.setLong(i, lngVal);
				} else if (val.getClass().toString().contains("Boolean")) {
					Boolean blnVal = (Boolean) val;
					ps.setBoolean(i, blnVal);
				} else if (val.getClass().toString().contains("Int")) {
					Integer intVal = (Integer) val;
					ps.setInt(i, intVal);
				} else if (val.getClass().toString().contains("BigDecimal")
						|| val.getClass().toString().contains("Float")) {
					BigDecimal intVal = (BigDecimal) val;
					ps.setBigDecimal(i, intVal);
				} else if (val.getClass().toString().contains("Date") || val.getClass().toString().contains("Time")) {
					Date intVal = (Date) val;
					ps.setDate(i, new java.sql.Date(intVal.getTime()));
				}
			} else {
				ps.setNull(i, Types.CHAR);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
