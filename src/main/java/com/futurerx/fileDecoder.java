package com.futurerx;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.io.IOUtils;
import org.mozilla.universalchardet.UniversalDetector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.slf4j.profiler.Profiler;

// This class will convert the file with any format to UTF-8 format
public class fileDecoder {/*
	public String execute(String filePath){
		final Logger log = LoggerFactory.getLogger(fileDecoder.class);
		Boolean tempFile = false;
		try {
			  
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			Profiler decodingProfiler = new Profiler("Decoding");
			decodingProfiler.setLogger(log);
			decodingProfiler.start("Decoding start - " + sdf.format(new Date()));
			byte[] buf = new byte[4096];
			FileInputStream fis;
			fis = new FileInputStream(filePath);

			UniversalDetector detector = new UniversalDetector(null);
	
		    int nread;
		    while ((nread = fis.read(buf)) > 0 && !detector.isDone()) {
		      detector.handleData(buf, 0, nread);
		    }

		    detector.dataEnd();
	
		    String encoding = detector.getDetectedCharset();
		    detector.reset();
		    fis.close();
		    if (encoding != null) {
		      log.info("Detected encoding = " + encoding);
		      System.out.println("Detected encoding = " + encoding);
		    } else {
		    	log.info("No encoding detected.");
		    	System.out.println("No encoding detected.");
		    }
	
		    detector.reset();
		    
		    if(encoding != null && !(encoding.equalsIgnoreCase("UTF-8") || encoding.equalsIgnoreCase("UTF8"))){
				InputStream inputStream1 = new FileInputStream(filePath);
				InputStreamReader inputStreamReader1 = new InputStreamReader(inputStream1, encoding);
				BufferedReader bufferedReader1 = new BufferedReader(inputStreamReader1);
				String line;	
				String tempFileLocation = filePath.substring(0, filePath.length()-4) + "_temp.txt";
			    FileOutputStream  fo = new FileOutputStream(new File(tempFileLocation));
				while((line = bufferedReader1.readLine()) != null){
					IOUtils.write(line + "\r\n", fo, "UTF8");
				}
		        fo.close();
		        bufferedReader1.close();

		        filePath = tempFileLocation;
		        tempFile = true;
				
		    }
		    decodingProfiler.stop().print();
		    log.info(decodingProfiler.toString());
		}catch(Exception e){
			e.printStackTrace();
		}
		return filePath;
	}
*/}
