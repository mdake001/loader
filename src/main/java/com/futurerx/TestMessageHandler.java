package com.futurerx;


import java.util.Map;

import com.futurerx.messaging.MessageConsumer;
import com.futurerx.messaging.MessageHandler;

public class TestMessageHandler implements MessageHandler {

	@Override
	public void init(Map<String, String> config) {
	}

	@Override
	public int handle(Object payload) {
		ParentObject parent = (ParentObject)payload;
		System.out.println(parent.getAtt1() + ":" + parent.getAtt2() + ":" + parent.getAtt3() + ":" + parent.getAttr4() + ":" + parent.getChildren().get(0).getAttr4());
		return MessageConsumer.PROCEED;
		//System.out.println(payload);
	}
}
