package com.futurerx.stage.models;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProviderAddressSPILoad {
	private Long id;
	private String addressId;
	private String hcid;
	private Date activeEndDate;
	private String cancel;
	private String ccr;
	private Date census;
	private String change;
	private String companyCount;
	private Date deltaDate;
	private String eligibility;
	private String medHistory;
	private String new1;
	private String providerId;
	private String reSupp;
	private String recordType;
	private String refill;
	private String rxFill;
	private String serviceLevel10;
	private String serviceLevel11;
	private String serviceLevel12;
	private String serviceLevel13;
	private String serviceLevel14;
	private String serviceLevel15;
	private String serviceLevelCode;
	private String spiLocationCode;
	private String spiRoot;
	private String tierCode;
	private String verificationCode;
	private Date verificationDate;
	private Integer dataIntakeFileId;
	private Long recordNumber;
	private String loaderCompositeKey;
}
