package com.futurerx.stage.models.pharmacy;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProviderRelationshipInformationLoad {

	private Long id;
	private String ncpdpproviderid;
	private Date effectiveFromDate;
	private Date effectiveThroughDate;
	private String filler;
	private String isPrimary;
	private String paymentCenterId;
	private String providerType;
	private String relationshipId;
	private String remitAndReconciliationId;
	private Integer dataIntakeFileId;
	private Long recordNumber;
	private String loaderCompositeKey;
	private String insertUser;
	private Date insertDateTime;
}
