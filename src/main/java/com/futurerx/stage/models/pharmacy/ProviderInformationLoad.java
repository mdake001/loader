package com.futurerx.stage.models.pharmacy;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ProviderInformationLoad {

	private Long id;
	private String ncpdpproviderid;
	private String contactEmailAddress;
	private String contactExtension;
	private String contactFirstName;
	private String contactLastName;
	private String contactMiddleInitial;
	private String contactPhoneNumber;
	private String contactTitle;
	private Date deaExpirationDate;
	private String deaRegistrationId;
	private String deactivationCode;
	private String dispenserClassCode;
	private String doctorName;
	private String federalTaxidnumber;
	private String filler;
	private String legalBusinessName;
	private String mailingAddress1;
	private String mailingAddress2;
	private String mailingAddressCity;
	private String mailingAddressStateCode;
	private String mailingAddressZipCode;
	private String medicareProviderid;
	private String npi;
	private String name;
	private String physicalLocation24hourOperationFlag;
	private String physicalLocationAddress1;
	private String physicalLocationAddress2;
	private String physicalLocationCity;
	private String physicalLocationCongressionalVotingDistrict;
	private String physicalLocationCountryParish;
	private String physicalLocationCrossStreet;
	private String physicalLocationEmailAddress;
	private String physicalLocationExtension;
	private String physicalLocationFax;
	private String physicalLocationLanguageCode1;
	private String physicalLocationLanguageCode2;
	private String physicalLocationLanguageCode3;
	private String physicalLocationLanguageCode4;
	private String physicalLocationLanguageCode5;
	private String physicalLocationMsa;
	private String physicalLocationPmsa;
	private String physicalLocationPhoneNumber;
	private String physicalLocationProviderHours;
	private String physicalLocationStateCode;
	private Date physicalLocationStoreClosureDate;
	private Date physicalLocationStoreOpenDate;
	private String physicalLocationZipCode;
	private String primaryProviderTypeCode;
	private String reinstatementCode;
	private Date reinstatementDate;
	private String secondaryProviderTypeCode;
	private String stateIncomeTaxidnumber;
	private String storeNumber;
	private String tertiaryProviderTypeCode;
	private String transactionCode;
	private Date transactionDate;
	private Integer dataIntakeFileId;
	private Long recordNumber;
	private String loaderCompositeKey;
	private String insertUser;
	private Date insertDateTime;
}
