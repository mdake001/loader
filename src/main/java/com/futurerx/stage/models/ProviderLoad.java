package com.futurerx.stage.models;

// Generated Aug 16, 2012 4:06:21 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * ProviderLoad generated by hbm2java
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProviderLoad implements java.io.Serializable {

	private Long id;
	private String hcid;
	private String companyCount;
	private Date deltaDate;
	private String providerId;
	private String recordType;
	private String providerType;
	private Integer dataIntakeFileId;
	private Long recordNumber;
	private String loaderCompositeKey;
	
}
