package com.futurerx.stage.models;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProviderSpecialtyLoad {
	
	private Long id;
	private String hcid;
	private String specialtyDescription;
	private String companyCount;
	private Date deltaDate;
	private String providerId;
	private String specialtyRank;
	private String recordType;
	private String tierCode;
	private String verificationCode;
	private Date verificationDate;
	private Integer dataIntakeFileId;
	private Long recordNumber;
	private String loaderCompositeKey;
	private String insertUser;
	private Date insertDateTime;

}
