package com.futurerx.stage.models;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProviderStateLicenseLoad {

	private Long id;
	private String hcid;
	private String licenseNumber;
	private String licenseState;
	private Date licenseExpirationDate;
	private Date licenseIssueDate;
	private String companyCount;
	private Date deltaDate;
	private String providerId;
	private String recordType;
	private String tierCode;
	private String verificationCode;
	private Date verificationDate;
	private Integer dataIntakeFileId;
	private Long recordNumber;
	private String loaderCompositeKey;
	private String insertUser;
	private Date insertDateTime;
}
