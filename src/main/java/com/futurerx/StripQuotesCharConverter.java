package com.futurerx;

import java.text.ParseException;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.blackbear.flatworm.ConversionOption;
import com.blackbear.flatworm.converters.CoreConverters;


public class StripQuotesCharConverter extends CoreConverters{
    
	public String stripChar(String str, Map<String, ConversionOption> options) throws ParseException {
        str = StringUtils.remove(str, '"');
        str = StringUtils.trim(str);
        if (!StringUtils.isBlank(str)) {
        	return convertChar(str, options);
		}
		return null;
    }
}
