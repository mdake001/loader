package com.futurerx;


public class TestMain {
	public static void main(String[] args) {
		Long end = 1005400000L;
		Long srt = 1000000000L;
		String timeTake = String.valueOf(Math.round(((end-srt))/(60 * 60 * 1000))) + "h " + 
				String.valueOf(Math.round(((end-srt))/(60 * 1000))%60) + "m " + 
				String.valueOf(((end-srt)/1000)%60) + "s";
		System.out.println("Time taken: " + timeTake);
	}
}
