package com.futurerx;
import java.util.HashMap;
import java.util.Map;

import org.apache.activemq.ActiveMQConnection;

import com.futurerx.messaging.MessageConsumer;
import com.futurerx.messaging.MessageHandler;
import com.futurerx.messaging.activemq.ActivemqMessageConsumer;

public class TestMessageConsumer {
	public static void main(String[] argv) throws Exception {
		MessageConsumer consumer = new ActivemqMessageConsumer();
		Map<String, String> config = new HashMap<String, String>();
		config.put("host", ActiveMQConnection.DEFAULT_BROKER_URL);
		
		MessageHandler handler = new TestMessageHandler();
		
		//consumer.init("TEST", "TestMessageHandler", null, config);
		consumer.init("TEST", handler, config);
		//consumer.consume();
	}
}
