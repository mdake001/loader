package com.futurerx.messaging;

import java.util.Map;

public interface MessageConsumer {
	public static final int PROCEED = 1;
	public static final int STOP = 2;
	public void init(String queueName, MessageHandler handler, Map<String, String> config) throws MessagingException;
	public void init(String queueName, String handlerClassName, Map<String, String> handlerConfig, Map<String, String> config) throws MessagingException;
	public void consume() throws MessagingException;
	public void close() throws MessagingException;
}
