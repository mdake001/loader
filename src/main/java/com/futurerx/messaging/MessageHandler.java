package com.futurerx.messaging;

import java.util.Map;

public interface MessageHandler {
	public void init(Map<String, String> config);
	public int handle(Object payload);
}
