package com.futurerx.messaging.rabbitmq;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Map;

import com.futurerx.messaging.MessageConsumer;
import com.futurerx.messaging.MessageHandler;
import com.futurerx.messaging.MessagingException;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class RabbitmqMessageConsumer implements MessageConsumer {
	private Channel channel;
    private Connection connection;
    private String queueName = null;
    private MessageHandler handler;
	@Override
	public void init(String queueName, MessageHandler handler, Map<String, String> config) throws MessagingException {
		this.handler = handler;
		this.queueName = queueName;
		String hostName = config.get("host");
	    ConnectionFactory factory = new ConnectionFactory();
	    factory.setHost(hostName);
		try {
			connection = factory.newConnection();
		} catch (IOException e) {
			throw new MessagingException(e);
		}
		try {
			channel = connection.createChannel();
		} catch (IOException e) {
			throw new MessagingException(e);
		}
	    
	    try {
			channel.queueDeclare(queueName, true, false, false, null);
		} catch (IOException e) {
			throw new MessagingException(e);
		}
		try {
			channel.basicQos(1);
		} catch (IOException e) {
			throw new MessagingException(e);
		}
	}

	@Override
	public void consume()
			throws MessagingException {
	    try {
			channel.basicConsume(this.queueName, false,
				     new DefaultConsumer(channel) {
				         @Override
				         public void handleDelivery(String consumerTag,
				                                    Envelope envelope,
				                                    AMQP.BasicProperties properties,
				                                    byte[] body)
				             throws IOException
				         {
				             String routingKey = envelope.getRoutingKey();
				             String contentType = properties.getContentType();
				             long deliveryTag = envelope.getDeliveryTag();
				             String message = new String(body);
				             
				         	 Gson gson = new Gson();

				             Type collectionType = new TypeToken<Object>(){}.getType();
				             Object payload = gson.fromJson(message, collectionType);
				             handler.handle(payload);
				             this.getChannel().basicAck(deliveryTag, false);
				         }
				     });
		} catch (IOException e) {
			throw new MessagingException(e);
		}
	}

	@Override
	public void init(String queueName, String handlerClassName,
			Map<String, String> handlerConfig, Map<String, String> config)
			throws MessagingException {
		Class handlerClass = null;
		try {
			handlerClass = Class.forName(handlerClassName);
		} catch (ClassNotFoundException e) {
			throw new MessagingException(e);
		}
		MessageHandler handler = null;
		try {
			handler = (MessageHandler)handlerClass.newInstance();
		} catch (InstantiationException e) {
			throw new MessagingException(e);
		} catch (IllegalAccessException e) {
			throw new MessagingException(e);
		}
		handler.init(handlerConfig);
		this.init(queueName, handler, config);
	}
	
	@Override
	public void close() throws MessagingException {
	    try {
			channel.close();
		} catch (IOException e) {
			throw new MessagingException(e);
		}
	    try {
			connection.close();
		} catch (IOException e) {
			throw new MessagingException(e);
		}
	}
}
