package com.futurerx.messaging.rabbitmq;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.futurerx.messaging.MessagePublisher;
import com.futurerx.messaging.MessagingException;
import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

public class RabbitmqMessagePublisher implements MessagePublisher {
	private Channel channel;
    private Connection connection;
    private String queueName = null;
	@Override
	public void init(String queueName, Map<String, String> config) throws MessagingException {
		this.queueName = queueName;
		String hostName = config.get("host");
	    ConnectionFactory factory = new ConnectionFactory();
	    factory.setHost(hostName);
		try {
			connection = factory.newConnection();
		} catch (IOException e) {
			throw new MessagingException(e);
		}
		try {
			channel = connection.createChannel();
		} catch (IOException e) {
			throw new MessagingException(e);
		}
	    
	    try {
			channel.queueDeclare(queueName, true, false, false, null);
		} catch (IOException e) {
			throw new MessagingException(e);
		}
	}

	@Override
	public void publish(Object payload)
			throws MessagingException {
		Gson gson = new Gson();
		String json = gson.toJson(payload);  
	    try {
			channel.basicPublish( "", this.queueName, 
			        MessageProperties.PERSISTENT_TEXT_PLAIN,
			        json.getBytes());
		} catch (IOException e) {
			throw new MessagingException(e);
		}
	}

	@Override
	public void publish(List<Object> payload)
			throws MessagingException {
		for(Object val : payload)
		{
			this.publish(val);
		}
	}

	@Override
	public void close() throws MessagingException {
	    try {
			channel.close();
		} catch (IOException e) {
			throw new MessagingException(e);
		}
	    try {
			connection.close();
		} catch (IOException e) {
			throw new MessagingException(e);
		}
	}
}
