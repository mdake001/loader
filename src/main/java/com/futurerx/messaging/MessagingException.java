package com.futurerx.messaging;

public class MessagingException extends Exception {
	public MessagingException(Exception e)
	{
		super(e);
	}
}
