package com.futurerx.messaging.activemq;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import com.futurerx.messaging.MessagePublisher;
import com.futurerx.messaging.MessagingException;
import com.google.gson.Gson;

public class ActivemqMessagePublisher implements MessagePublisher {
    private Connection connection;
    private String queueName = null;
    private Destination destination;
    MessageProducer producer;
    Session session;
    boolean transacted = false;
    long batchSize = 100;
    long currentMessageCount = 0;
	@Override
	public void init(String queueName, Map<String, String> config) throws MessagingException {
		this.queueName = queueName;
		String hostName = config.get("host");
		String transacted = config.get("transacted");
		if(transacted != null && transacted.equals("true"))
		{
			this.transacted = true;
		}
		String batchSize = config.get("transaction_batch_size");
		if(batchSize != null)
		{
			this.batchSize = Long.valueOf(batchSize).longValue();
		}
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(hostName);
        try {
			connection = connectionFactory.createConnection();
			connection.start();
			session = connection.createSession(this.transacted, Session.AUTO_ACKNOWLEDGE);
			destination = session.createQueue(queueName);
			producer = session.createProducer(destination);
	        producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
		} catch (JMSException e) {
			throw new MessagingException(e);
		}
        System.out.println("BatchSize:" + this.transacted + ":" + this.batchSize);
	}

	@Override
	public void publish(Object payload)
			throws MessagingException {
        ObjectMessage message;
		try {
			message = session.createObjectMessage((Serializable)payload);
			//System.out.println(payload);
	        producer.send(message);
	        this.currentMessageCount++;
	        if(this.transacted)
	        {
	        	if(this.currentMessageCount == this.batchSize)
	        	{
	        		System.out.println("Commiting...");
	        		session.commit();
	        		this.currentMessageCount = 0;
	        	}
	        }
		} catch (JMSException e) {
			throw new MessagingException(e);
		}
	}

	@Override
	public void publish(List<Object> payload)
			throws MessagingException {
		for(Object val : payload)
		{
			this.publish(val);
		}
	}

	@Override
	public void close() throws MessagingException {
		try {
	        if(this.transacted)
	        {
	        	{
	        		System.out.println("Commiting...");
	        		session.commit();
	        		this.currentMessageCount = 0;
	        	}
	        }
			session.close();
			connection.close();
		} catch (JMSException e) {
			throw new MessagingException(e);
		}
	}
}
