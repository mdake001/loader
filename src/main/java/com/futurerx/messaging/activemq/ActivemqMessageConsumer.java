package com.futurerx.messaging.activemq;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Map;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import com.futurerx.messaging.MessageConsumer;
import com.futurerx.messaging.MessageHandler;
import com.futurerx.messaging.MessagingException;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class ActivemqMessageConsumer implements MessageConsumer {
    private Connection connection;
    private String queueName = null;
    private Destination destination;
    Session session;
    private MessageHandler handler;
    javax.jms.MessageConsumer consumer = null;
    boolean transacted = false;
    long batchSize = 1;
    long currentMessageCount = 0;
    long wait = -1;
	@Override
	public void init(String queueName, MessageHandler handler, Map<String, String> config) throws MessagingException {
		this.handler = handler;
		this.queueName = queueName;
		String hostName = config.get("host");
		String transacted = config.get("transacted");
		if(transacted != null && transacted.equals("true"))
		{
			this.transacted = true;
		}
		String batchSize = config.get("transaction_batch_size");
		if(batchSize != null)
		{
			this.batchSize = Long.valueOf(batchSize).longValue();
		}
		String waitStr = config.get("wait");
		if(waitStr != null)
		{
			this.wait = new Long(waitStr).longValue();
		}
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(hostName);
        try {
			connection = connectionFactory.createConnection();
			connection.start();
			session = connection.createSession(this.transacted, Session.AUTO_ACKNOWLEDGE);
			destination = session.createQueue(queueName);
            consumer = session.createConsumer(destination);
            //consumer.setMessageListener(this);
            Message message;
            while (true) {
            	if (this.wait == 0) {
            		message = consumer.receive(1000);
            	}
            	else {
                	message = consumer.receive();
            	}
            	//System.out.println("Message:" + message + ":" + this.wait);
            	if(message == null){
            		break;
            	}
                int status = onMessage(message);
                if(status == MessageConsumer.STOP)
                {
                	return;
                }
            }

		} catch (JMSException e) {
			throw new MessagingException(e);
		}
	}

	@Override
	public void consume()
			throws MessagingException {
	}

	@Override
	public void init(String queueName, String handlerClassName,
			Map<String, String> handlerConfig, Map<String, String> config)
			throws MessagingException {
		Class handlerClass = null;
		try {
			handlerClass = Class.forName(handlerClassName);
		} catch (ClassNotFoundException e) {
			throw new MessagingException(e);
		}
		MessageHandler handler = null;
		try {
			handler = (MessageHandler)handlerClass.newInstance();
		} catch (InstantiationException e) {
			throw new MessagingException(e);
		} catch (IllegalAccessException e) {
			throw new MessagingException(e);
		}
		handler.init(handlerConfig);
		this.init(queueName, handler, config);
	}
	
	@Override
	public void close() throws MessagingException {
			try {
				session.close();
				connection.close();
			} catch (JMSException e) {
				throw new MessagingException(e);
			}
	}

	private int onMessage(Message message) {
		//System.out.println(message);
		int status = MessageConsumer.PROCEED;
        if (message instanceof ObjectMessage) {
             try {
            	//System.out.println(((ObjectMessage) message).getObject());
            	 //System.out.println(this.handler + ":" + message);
            	if(this.handler != null)
            	{
					status = handler.handle(((ObjectMessage) message).getObject());
					if(status == MessageConsumer.STOP)
					{
						this.currentMessageCount = this.batchSize - 1;
					}
            	}
		        this.currentMessageCount++;
		        if(this.transacted)
		        {
		        	if(this.currentMessageCount >= this.batchSize)
		        	{
		        		session.commit();
		        		this.currentMessageCount = 0;
		        	}
		        }
			} catch (JMSException e) {
				throw new RuntimeException(e);
			}
        } else {
			throw new RuntimeException("Invalid Message Type");
        }
        return status;
	}
}
