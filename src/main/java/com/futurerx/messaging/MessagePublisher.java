package com.futurerx.messaging;

import java.util.List;
import java.util.Map;

public interface MessagePublisher {
	public void init(String queueName, Map<String, String> config) throws MessagingException;
	public void publish(Object payload) throws MessagingException;
	public void publish(List<Object> payload) throws MessagingException;
	public void close() throws MessagingException;
}
