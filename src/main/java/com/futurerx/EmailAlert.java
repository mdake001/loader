package com.futurerx;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.Security;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class EmailAlert {
//	private ApplicationContext aContext = new ClassPathXmlApplicationContext("applicationContext.xml");
//	private JdbcTemplate jdbcTemplate = null;
	private static Properties mailServerConfig = new Properties();

	public static void main(String args[]) {
		try {
			(new EmailAlert()).sendMessage("Test Subject", "Test message", args);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	public void sendMessage(String message) throws MessagingException {
		sendMessage(null, message, null);
	}

	public void sendMessage(String subject, String message) throws MessagingException {
		sendMessage(subject, message, null);
	}
	
	public void sendMessage(String subject, String message, String recipients[]) throws MessagingException {
	//	Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
		InputStream input = null;

		try {
			input = this.getClass().getClassLoader().getResourceAsStream(/*"src"+File.separator+"main"+File.separator+"resources"+File.separator+*/"emailConnection.properties");
			mailServerConfig.load(input);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		final String username = mailServerConfig.getProperty("username");
		final String password = mailServerConfig.getProperty("password");
		String from = mailServerConfig.getProperty("from");
		boolean debug = Boolean.parseBoolean(mailServerConfig.getProperty("mail.debug"));

		if (recipients == null) {
			recipients = mailServerConfig.getProperty("recipients").split(",");
		}
		if (subject == null || subject.length() == 0) {
			subject = mailServerConfig.getProperty("subject");
		}
		
		Session session = Session.getDefaultInstance(mailServerConfig, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		session.setDebug(debug);

		Message msg = new MimeMessage(session);
		InternetAddress addressFrom = new InternetAddress(from);
		msg.setFrom(addressFrom);

		InternetAddress[] addressTo = new InternetAddress[recipients.length];
		for (int i = 0; i < recipients.length; i++) {
			addressTo[i] = new InternetAddress(recipients[i]);
		}
		msg.setRecipients(Message.RecipientType.TO, addressTo);

		// Setting the Subject and Content Type
		msg.setSubject(subject);
		msg.setContent(message, "text/plain");
		Transport.send(msg);
	}
}
