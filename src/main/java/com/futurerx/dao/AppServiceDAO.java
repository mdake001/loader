package com.futurerx.dao;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class AppServiceDAO extends HibernateDaoSupport {
	public void save(Object transientInstance) {
		try {
			getHibernateTemplate().save(transientInstance);
		} catch (RuntimeException re) {

			throw re;
		}
	}

	public void update(Object transientInstance) {
		try {
			getHibernateTemplate().saveOrUpdate(transientInstance);
			//System.out.println("Sucessfully Saved the Instance ");
		} catch (RuntimeException re) {
			re.printStackTrace();
			throw re;
		}
	}

	public void delete(Object transientInstance) {
		try {

			getHibernateTemplate().delete(transientInstance);
		} catch (RuntimeException re) {

			throw re;
		}
	}
	
	public <T> T findById(Class<T> x, java.lang.String id) {

		try {
			return getHibernateTemplate().get(x, id);

		} catch (RuntimeException re) {

			throw re;
		}
	}

	public <T> T findById(Class<T> x, java.lang.Long id) {

		try {
			return getHibernateTemplate().get(x, id);

		} catch (RuntimeException re) {

			throw re;
		}
	}
	public <T> T findById(Class<T> x, java.lang.Integer id) {

		try {

			return getHibernateTemplate().get(x, id);

		} catch (RuntimeException re) {

			throw re;
		}
	}
	
	public <T> List<T> findByIn(Class<T> x, String commaSeperated,String columnName) {

		try {
			getHibernateTemplate().setMaxResults(0);
			String modelName = x.getName().substring(
					x.getName().lastIndexOf('.') + 1);
			String queryString = "from " + modelName + " as model where " + columnName  + " in (" + commaSeperated +")";

			return getHibernateTemplate().find(queryString);

		} catch (RuntimeException re) {

			throw re;
		}
	}


	public <T> List<T> findByCriteria(Class<T> x, Map<String, Object> map) {
		try {
			getHibernateTemplate().setMaxResults(0);
			String modelName = x.getName().substring(
					x.getName().lastIndexOf('.') + 1);
			String queryString = "from " + modelName + " as model where ";
			Object[] value = new Object[map.size()];
			int i = 0;
			for (String s : map.keySet()) {
				if (i > 0) {
					queryString = queryString + " and ";
				}
				queryString = queryString + s + "= ? ";
				value[i] = map.get(s);
				i++;
			}

			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	public <T> List<T> findByCriteria(Class<T> x, Map<String, Object> map,HashSet<String> likeSearch) {
		try {
			getHibernateTemplate().setMaxResults(0);
			String modelName = x.getName().substring(
					x.getName().lastIndexOf('.') + 1);
			String queryString = "from " + modelName + " as model where ";
			Object[] value = new Object[map.size()];
			int i = 0;
			for (String s : map.keySet()) {
				if (i > 0) {
					queryString = queryString + " and ";
				}
				
				if (likeSearch.contains(s)) {
					queryString = queryString + s + " like ? ";
					value[i] = "%" +(String)map.get(s) + "%";
				} else {
					queryString = queryString + s + "= ? ";
					value[i] = map.get(s);
				}
				i++;
			}

			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	public <T> List<T> findByCriteria(Class<T> x, Map<String, Object> map,String orderBy) {
		try {
			getHibernateTemplate().setMaxResults(0);
			String modelName = x.getName().substring(
					x.getName().lastIndexOf('.') + 1);
			String queryString = "from " + modelName + " as model where ";
			Object[] value = new Object[map.size()];
			int i = 0;
			for (String s : map.keySet()) {
				if (i > 0) {
					queryString = queryString + " and ";
				}
				queryString = queryString + s + "= ? ";
				value[i] = map.get(s);
				i++;
			}
			queryString = queryString + " " + orderBy;
			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	public <T> List<T> findByCriteria(Class<T> x, Map<String, String> map,HashMap<String,String> operator,String joinOperation) {
		try {
			getHibernateTemplate().setMaxResults(0);
			String modelName = x.getName().substring(
					x.getName().lastIndexOf('.') + 1);
			String queryString = "from " + modelName + " as model where ";
			Object[] value = new Object[map.size()];
			String jOp=" and ";
			if (joinOperation != null) {
				jOp=joinOperation;
			}
			int i = 0;
			for (String s : map.keySet()) {
				if (i > 0) {
					
					queryString = queryString + jOp;
				}
				String op="=";
				if (operator.containsKey(s)) {
					op=operator.get(s);
				}
				queryString = queryString + s +  " " + op + " ? ";
				value[i] = map.get(s);
				i++;
			}
			
			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	public <T> List<T> findByCriteria(Class<T> x, Map<String, String> map,HashMap<String,String> operator,String joinOperation,String prependOperation) {
		try {
			getHibernateTemplate().setMaxResults(0);
			String modelName = x.getName().substring(
					x.getName().lastIndexOf('.') + 1);
			String queryString = "from " + modelName + " as model where ( " + prependOperation + " ) and ( " ;
			Object[] value = new Object[map.size()];
			String jOp=" and ";
			if (joinOperation != null) {
				jOp=joinOperation;
			}
			int i = 0;
			for (String s : map.keySet()) {
				if (i > 0) {
					
					queryString = queryString + jOp;
				}
				String op="=";
				if (operator.containsKey(s)) {
					op=operator.get(s);
				}
				queryString = queryString + s +  " " + op + " ? ";
				value[i] = map.get(s);
				i++;
			}
			if (value.length > 0) {
				queryString=queryString + " )";
			} else {
				queryString=queryString + " 1=1) ";
			}

			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public <T> List<T> findByQueryString(Class<T> x, String queryString,
			Object[] value,int maxResults) {
		try {
			// String queryString = "from " + modelName + " as model where ";
			getHibernateTemplate().setMaxResults(maxResults);
			return getHibernateTemplate().find(queryString, value);
			
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	public <T> List<T> findByQueryString(Class<T> x, String queryString,
			Object[] value) {
		try {
			// String queryString = "from " + modelName + " as model where ";
			return getHibernateTemplate().find(queryString, value);
			
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	public <T> List<T> findByQueryString(Class<T> x, String queryString
			) {
		try {
			// String queryString = "from " + modelName + " as model where ";
			return getHibernateTemplate().find(queryString);
			
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	public List findByQueryString(String queryString) {
		try {
			// String queryString = "from " + modelName + " as model where ";
			return getHibernateTemplate().find(queryString);
			
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	public List findByQueryString(String queryString,Object[] value) {
		try {
			// String queryString = "from " + modelName + " as model where ";
			return getHibernateTemplate().find(queryString,value);
			
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public <T> List<T> findByNamedQuery(Class<T> x, final String queryName,
			final int maxResults) throws DataAccessException {
		return (List<T>) getHibernateTemplate().executeWithNativeSession(
				new HibernateCallback() {
					@Override
					public Object doInHibernate(Session session)
							throws HibernateException {
						Query queryObject = session.getNamedQuery(queryName);
						queryObject.setMaxResults(maxResults);
						return queryObject.list();
					}
				});
	}

	public <T> List<T> findAll(Class<T> x) {
		try {

			String modelName = x.getName().substring(
					x.getName().lastIndexOf('.') + 1);
			String queryString = "from " + modelName + " as model ";

			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	
	public Long getCount(String hql,Object[] value) {
		  Long count = (Long)getHibernateTemplate().find(hql,value).listIterator().next();
		  return count.longValue();

	}

}
