package com.futurerx;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang.StringUtils;



public class StripQuotesDateConverter extends DateConverter{
    public Date stripDate(String str, Map<String, String> options) throws ParseException {
		str = StringUtils.remove(str, '"');
		if (!StringUtils.isBlank(str)) {
        	try {
				return convertDate(str, options);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} 
		return null;
    }
}
