package com.futurerx;

public interface StageModel {
	public Integer getDataIntakeFileId();
	public Long getRecordNumber();
}
