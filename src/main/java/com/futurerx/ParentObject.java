package com.futurerx;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


public class ParentObject implements Serializable{
	private String att1;
	private Integer att2;
	private int att3;
	private Date attr4;
	public Date getAttr4() {
		return attr4;
	}
	public void setAttr4(Date attr4) {
		this.attr4 = attr4;
	}
	private List<ChildObject> children;

	public String getAtt1() {
		return att1;
	}
	public List<ChildObject> getChildren() {
		return children;
	}
	public void setChildren(List<ChildObject> children) {
		this.children = children;
	}
	public void setAtt1(String att1) {
		this.att1 = att1;
	}
	public Integer getAtt2() {
		return att2;
	}
	public void setAtt2(Integer att2) {
		this.att2 = att2;
	}
	public int getAtt3() {
		return att3;
	}
	public void setAtt3(int att3) {
		this.att3 = att3;
	}
}
