package com.futurerx;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.blackbear.flatworm.ConversionOption;


public class DateConverter {
	static List<String> dateFormats = new ArrayList<String>();

	public Date convertDate(String str, Map<String, String> options) throws ParseException {
		DateFormat sdf = null;
		Date retVal = null;
		if (!str.isEmpty() && !str.equalsIgnoreCase("NULL") && !str.equalsIgnoreCase("00000000")) {
			if (options.containsKey("format")) {
				try{
					for (Map.Entry<String, String> entry : options.entrySet()) {
						Object newFormat = entry.getValue();
						ConversionOption op = (ConversionOption)newFormat;
						sdf = new SimpleDateFormat(op.getValue());
						retVal = sdf.parse(str);
					}

				}catch (Exception e){
					retVal = getDateFormat(str);
				}
			}else{			
				retVal = getDateFormat(str);
				
			}
		}
		return retVal;
    }
   
	private Date getDateFormat(String str) {
		// TODO Auto-generated method stub
		SimpleDateFormat sdf = null;
		Date retVal = null;
		for (String format : dateFormats) {
			sdf = new SimpleDateFormat(format);
			try {
				retVal = sdf.parse(str);
			} catch (Exception e) {
				continue;
			}
			break;
		}
		return retVal;
	}

	static {
		dateFormats.add("MMddyyyy HH:mm:ss");
		dateFormats.add("MM/dd/yyyy HH:mm:ss");
		dateFormats.add("MMddyyyy HH:mm");
		dateFormats.add("yyyy-MM-dd HH:mm:ss");
		dateFormats.add("yyyy-MM-dd");
		dateFormats.add("yyyy/MM/dd");
		dateFormats.add("MM/dd/yyyy");
		dateFormats.add("MM-dd-yyyy");
		dateFormats.add("dd-MMM-yy");
		dateFormats.add("MMddyyyy");
		dateFormats.add("yyyyMMdd");
		dateFormats.add("EEE, dd MMM yyyy HH:mm:ss");
		dateFormats.add("dd-MMM-yy");
		
//		dateFormats.add("MM/dd/yyyy");
	}

	

}
