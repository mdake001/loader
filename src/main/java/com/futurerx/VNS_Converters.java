package com.futurerx;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.blackbear.flatworm.converters.CoreConverters;
import com.blackbear.flatworm.errors.FlatwormConversionException;


public class VNS_Converters extends CoreConverters {
	private static Map<String,String> genderMap = null;
	private static Map<String,String> languagesMap = null;
	private static Map<String,String> racesMap = null;
	private static Map<String,String> racesSourceMap = null;
	private static Map<String,String> languagesSourceMap = null;
	private static HashMap<String, String> signedOverpunch = new HashMap<String, String>();

	public BigDecimal fixFloat(String str,  Map<?, ?> options) {
		Float retVal = null;
		Integer i = (Integer) options.get("round");
		if (i == null) {
			i = 2;
		}
		if (str != null && str.length() > 0) {
			String overpunch = signedOverpunch.get(str.substring(str.length()-1));

			if (overpunch != null) {
				str = str.replace(str.substring(str.length()-1), overpunch.split(",")[0]);
				retVal = new Float(str) * Integer.parseInt(overpunch.split(",")[1]);
			}
			else {
				retVal = new Float(str);
			}
			retVal = (float) (retVal / (Math.pow(10, i)));
		}
		else
			retVal = 0F;
		return new BigDecimal(Float.toString(retVal));
	}
	
	public String convertEthnicity(String str, Map<?, ?> options) throws FlatwormConversionException {
		String res;
		if (str != null && (str.equals("Y") 
				|| str.equals("y")
				|| str.equals("T")
				|| str.equals("t")
				|| str.equals("1"))) {
			res = "Hispanic";
		}
		else {
			res = "";
		}
		return res;
    }
	
	public String convertLanguage(String str, Map<?, ?> options) throws FlatwormConversionException {
		String res = languagesMap.get(str);
		return res;
    }
	
	public String convertLanguageSource(String str, Map<?, ?> options) throws FlatwormConversionException {
		String res = languagesSourceMap.get(str);
		return res;
    }
	
	public String convertGender(String str, Map<?, ?> options) throws FlatwormConversionException {
		String res = genderMap.get(str);
		return res;
    }
	
	public Float convertFloatOverpunch(String str, Map<?, ?> options) throws FlatwormConversionException {
		Float res = null;
		return res;
    }
	
	static {
		genderMap = new HashMap<String, String>();
		genderMap.put("M", "Male");
		genderMap.put("F", "Female");		
		genderMap.put("1", "Male");
		genderMap.put("2", "Female");		

		languagesMap = new HashMap<String, String>();
		languagesMap.put("0","English");
		languagesMap.put("1","Non-English");
		languagesMap.put("2","Unknown");
		languagesMap.put("3","Declined");

		racesMap = new HashMap<String, String>();
		racesMap.put("0","Unknown");
		racesMap.put("1","White");
		racesMap.put("2","Black");
		racesMap.put("3","Other");
		racesMap.put("4","Asian");
		racesMap.put("5","Hispanic");
		racesMap.put("6","North American Native");
		racesMap.put("7","Hawaiian");
		racesMap.put("8","Mixed");
		racesMap.put("9","Declined");

		racesSourceMap = new HashMap<String, String>();
		racesSourceMap.put("0","Direct");
		racesSourceMap.put("1","CMS Database");
		racesSourceMap.put("2","State Database");
		racesSourceMap.put("3","Surname Analysis");
		racesSourceMap.put("4","Geo-coding Analysis");
		racesSourceMap.put("5","Other");

		languagesMap = new HashMap<String, String>();
		languagesMap.put("0","English");
		languagesMap.put("1","Non-English");
		languagesMap.put("2","Unknown");
		languagesMap.put("3","Declined");

		languagesSourceMap = new HashMap<String, String>();
		languagesSourceMap.put("0","Direct");
		languagesSourceMap.put("1","Indirect - state Medicaid agency");
		languagesSourceMap.put("2","Indirect - CMS");
		languagesSourceMap.put("3","Other");
		
		signedOverpunch.put("}", "0,-1");
		signedOverpunch.put("J", "1,-1");
		signedOverpunch.put("K", "2,-1");
		signedOverpunch.put("L", "3,-1");
		signedOverpunch.put("M", "4,-1");
		signedOverpunch.put("N", "5,-1");
		signedOverpunch.put("O", "6,-1");
		signedOverpunch.put("P", "7,-1");
		signedOverpunch.put("Q", "8,-1");
		signedOverpunch.put("R", "9,-1");
		signedOverpunch.put("{", "0,1");
		signedOverpunch.put("A", "1,1");
		signedOverpunch.put("B", "2,1");
		signedOverpunch.put("C", "3,1");
		signedOverpunch.put("D", "4,1");
		signedOverpunch.put("E", "5,1");
		signedOverpunch.put("F", "6,1");
		signedOverpunch.put("G", "7,1");
		signedOverpunch.put("H", "8,1");
		signedOverpunch.put("I", "9,1");

	}
}
