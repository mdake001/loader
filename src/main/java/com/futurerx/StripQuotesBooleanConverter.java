package com.futurerx;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.blackbear.flatworm.ConversionOption;

public class StripQuotesBooleanConverter {
    public Boolean stripBool(String str,  Map<String, ConversionOption> options) {
    	Boolean res = Boolean.FALSE;
        ConversionOption justifyOption = options.get("justify");
        str = StringUtils.remove(str, '"');
        if (!StringUtils.isBlank(str) && !(str.length() == 0)) {
        	if (str.equals("1") || str.equalsIgnoreCase("true") || str.equalsIgnoreCase("Y")  ) {
                if (justifyOption != null) {
                	str = str.trim();
                }
        		res = Boolean.TRUE; 
        	}
		}   	
		return res;
    }
}
